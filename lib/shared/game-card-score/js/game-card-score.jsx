import React from 'react';
import { generateBemCss } from 'project/helpers';

const GameCardScore = ({ children, childCss, bemList }) => {
    const rootCss = generateBemCss('game-card-score', bemList);

    return (
        <div className={`${childCss} ${rootCss}`}>
            {children}
        </div>
    );
};

GameCardScore.defaultProps = {
    childCss: '',
};

export default GameCardScore;
