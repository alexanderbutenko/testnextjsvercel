import React from 'react';
import PropTypes from 'prop-types';

class Pagination extends React.Component {
    renderPages(startIndex, endIndex, current) {
        const pages = [];

        if (startIndex > endIndex) return [];

        for (let i = startIndex; i <= endIndex; i++) {
            const pageStateCss = i === current ? 'is-current' : '';
            pages.push((
                <li className="pagination__li" key={i}>
                    <button
                        className={`pagination__page ${pageStateCss}`}
                        type="button"
                        disabled={i === current}
                        onClick={() => this.props.onSelectPage(i)}
                    >{i}</button>
                </li>
            ));
        }

        return pages;
    }

    render() {
        const { pagination, pagesToShow } = this.props;

        if (!pagination ||
            typeof pagination.current !== 'number' ||
            typeof pagination.total !== 'number'
        ) {
            return null;
        }

        let startIndex = pagination.current;
        let endIndex = pagination.current + pagesToShow - 1;

        if (endIndex > pagination.total) {
            endIndex = pagination.total;

            if (endIndex - startIndex !== pagesToShow) {
                startIndex = endIndex - (pagesToShow - 1);
            }
        }

        if (startIndex < 1) {
            startIndex = 1;
        }

        const prevStateCss = startIndex === 1 ? 'is-disabled' : '';
        const nextStateCss = endIndex === pagination.total ? 'is-disabled' : '';

        return (
            <div className="pagination">
                <button
                    className={`pagination__page ${prevStateCss}`}
                    type="button"
                    disabled={startIndex === 1}
                    onClick={() => {
                        const index = startIndex - pagesToShow;
                        this.props.onSelectPage(index > 1 ? index : 1);
                    }}
                >Prev
                </button>
                <div className="pagination__divider"></div>
                <ul className="pagination__list">
                    {this.renderPages(startIndex, endIndex, pagination.current)}
                </ul>
                <div className="pagination__divider"></div>
                <button
                    className={`pagination__page ${nextStateCss}`}
                    type="button"
                    disabled={endIndex === pagination.total}
                    onClick={() => {
                        const index = endIndex + 1;
                        this.props.onSelectPage(index < pagination.total ? index : pagination.total);
                    }}
                >Next
                </button>
            </div>
        );
    }
}

Pagination.defaultProps = {
    onSelectPage: () => {},
    pagesToShow: 5,
};

Pagination.propTypes = {
    pagination: PropTypes.shape({
        current: PropTypes.number,
        total: PropTypes.number,
    }),
    pagesToShow: PropTypes.number,
    onSelectPage: PropTypes.func,
};

export default Pagination;
