import React from 'react';
import PropTypes from 'prop-types';
import { generateBemCss, getBemList } from 'project/helpers';
import { matchStatuses } from 'project/general';
import { SpriteSvg } from 'project/sprite-svg';
import { PartnerHeading, promoTypes } from 'components/partner-heading';
import { GameClock } from 'components/game-clock';

class GameCardClock extends React.Component {
    render() {
        const { match } = this.props;

        if (!match) return null;

        let extraMod;

        if (match.status !== matchStatuses.FULLTIME && match.isExtraTime) {
            extraMod = 'extra-time';
        }

        const rootCss = generateBemCss('game-card-clock', getBemList(this.props.bemList, [extraMod]));

        return (
            <div className={rootCss}>
                {
                    match.status === matchStatuses.HALFTIME &&
                    <div className="game-card-clock__time">HT</div>
                }
                {
                    match.status === matchStatuses.FULLTIME &&
                    <div className="game-card-clock__time">FT</div>
                }
                {
                    match.status !== matchStatuses.HALFTIME &&
                    match.status !== matchStatuses.FULLTIME &&
                    <>
                        <SpriteSvg iconName={'clock'} width={12} height={12} />
                        <div className="game-card-clock__time">
                            <GameClock
                                minutes={match.minute}
                                seconds={match.second}
                                isTimerRunning={match.timerRunning}
                                isLive={this.props.isLive}
                            />
                        </div>
                    </>
                }
                <PartnerHeading
                    childCss={'game-card-clock__partner'}
                    bemList={['clock']}
                    type={promoTypes.MATCH_GAME_CLOCK}
                />
            </div>
        );
    }
}

GameCardClock.defaulProps = {
    GameCardClock: [],
};

GameCardClock.propTypes = {
    bemList: PropTypes.array,
};

export default GameCardClock;
