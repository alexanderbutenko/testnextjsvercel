import React from 'react';
import { ArrayHelper } from '@/lib/foundation/helpers';
import { generateBemCssModule, getBemList } from '@/lib/project/helpers';
import { contentTypes } from '@/lib/project/general';
import { twitterLoader } from '@/lib/project/loaders';
import { Richtext } from '@/components/richtext';
import { Quote } from '@/components/quote';
import { Image } from '@/components/image';
import { Gallery } from '@/components/gallery';
import { Video } from '@/components/video';
import { VideoCarousel } from '@/components/video-carousel';
import { LatestNewsCarousel } from '@/components/latest-news-carousel';
import styles from '../scss/article-content.module.scss';

class ArticleContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            twitterHasLoaded: false,
        };
        this.rootRef = React.createRef();
        this.unsubscribeOnLoad = () => {
        };
    }

    componentDidMount() {
        this.unsubscribeOnLoad = twitterLoader.subscribeOnLoad(
            () => this.setState({ twitterHasLoaded: true })
        );
        window.addEventListener('load', this.handleLoad);
    }

    handleLoad() {
        let a = document.querySelectorAll('.richtext a');
        if (a.length > 0) {
            a.forEach(link => link.setAttribute('target', '_blank'));
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.twitterHasLoaded !== this.state.twitterHasLoaded &&
            this.state.twitterHasLoaded &&
            this.rootRef.current
        ) {
            window.twttr.widgets.load(this.rootRef.current);
        }
    }

    componentWillUnmount() {
        this.unsubscribeOnLoad();
        window.removeEventListener('load', this.handleLoad)
    }

    getConfigurableCarousel(id) {
        if (!id) return null;

        const { configurableCarousels } = this.props.article;

        if (ArrayHelper.isEmpty(configurableCarousels)) return null;

        const foundConfigurableCarousels = configurableCarousels.find((configurableCarousel) => {
            return configurableCarousel.id === id;
        });

        return foundConfigurableCarousels || null;
    }

    getArticleInfo = () => {
        const { heroMedia, url } = this.props.article;

        return {
            title: heroMedia.title,
            url: url || document.location.href,
        };
    };

    renderContent(content) {
        let configurableCarousel;

        switch (content.contentType) {
            case contentTypes.TEXT:
                return <Richtext model={content} />;
            case contentTypes.QUOTE:
                return <Quote model={content} />;
            case contentTypes.IMAGE:
                return <Image model={content} />;
            case contentTypes.GALLERY:
                return <Gallery model={content} />;
            case contentTypes.VIDEO:
                return <Video
                    model={content}
                    gtm={this.getArticleInfo()}
                    bemList={content.title ? ['article', 'flexible'] : ['article']}
                />;
            // case contentTypes.PROMO:
            // return <Banner />;
            // case contentTypes.CLOUD_MATRIX:
            //     return <VideoCarousel
            //         endpointUrl={content.feedUrl}
            //         title={content.title}
            //         sponsor={content.sponsor}
            //         targetUrl={'/pro14tv/all-videos'}
            //     />;
            // case contentTypes.RELATED_CONTENT:
            //     configurableCarousel = this.getConfigurableCarousel(content.id);
            //
            //     if (!configurableCarousel) return null;
            //
            //     return <LatestNewsCarousel {...configurableCarousel} />;
            default:
                console.log('Unknown type:', content.contentType);
                return null;
        }
    }

    render() {
        const { article } = this.props;

        if (!article ||
            ArrayHelper.isEmpty(article.content) ||
            !this.state.twitterHasLoaded
        ) {
            return null;
        }

        const rootCss = generateBemCssModule('article-content', getBemList(this.props.bemList), styles);
        const modName = article.content.toLowerCase().replace('_', '-');

        const rowCss = generateBemCssModule('article-content__row', [modName], styles);
        const containerCss = generateBemCssModule('article-content__container', [modName], styles);
        return (
            <div className={rootCss} ref={this.rootRef}>
                <div className={rowCss}>
                    <div className={containerCss}>
                        {article.content}
                    </div>
                </div>
            </div>
        );
    }
}

export default ArticleContent;
