import React from 'react';
import { format, parseISO } from 'date-fns';
import { generateBemCss, generateThumbnailSrc } from '@/lib/project/helpers';
import { contentTypes } from '@/lib/project/general';
import { SpriteSvg } from '@/lib/project/sprite-svg';
import { articleModal } from '@/lib/project/article-modal';
import { DamImage } from '@/lib/shared/dam-image';

class NewsCard extends React.Component {


    render() {
        const { article, childCss, bemList } = this.props;

        if (!article) return null;

        const {
            displayCategory,
            heroMedia,
            publishDate,
            author,
        } = article;
        const { content } = heroMedia;
        const typeMod = content?.contentType.toLowerCase();
        const memberMod = this.getMemberModifier();
        const bemCss = generateBemCss('news-card', [...bemList, typeMod, memberMod]);

        const getImageUrlByType = () => {
            if (!content) return null;

            if (content.contentType === contentTypes.VIDEO) {
                return generateThumbnailSrc(content.sourceSystemId);
                //return content.videoThumbnail;
            }

            return content.image;
        };

        return (
            <a
                className={`${childCss} ${bemCss}`}
                href={`${article.url}`}
                onClick={(event) => {
                    event.preventDefault();
                    articleModal.open(article.url);
                }}
            >
                <div className="news-card__image of-cover">
                    <DamImage image={getImageUrlByType()} />
                    <SpriteSvg childCss="news-card__icon-play" iconName="play" width={18} height={16} />
                    <div className="news-card__member-logo of-contain">
                        <DamImage image={'c5fcf2d0-d489-4fa2-b8b4-1f267e871e29.png'} />
                    </div>
                </div>
                <div className="news-card__content">
                    <SpriteSvg childCss="news-card__icon-play-big" iconName="play" width={30} height={30} />
                    <div className="news-card__top-wrap">
                        {
                            displayCategory &&
                            <p className="news-card__label">{displayCategory.text}</p>
                        }
                        {
                            publishDate &&
                            <time className="news-card__date" dateTime={publishDate}>
                                {format(parseISO(publishDate), 'd MMM yyyy')}
                            </time>
                        }
                    </div>
                    <h3 className="news-card__title">{heroMedia.title}</h3>

                    <div className="news-card__bottom">
                        {
                            author &&
                            author.name &&
                            <div className="news-card__author">
                                {
                                    author.imageUrl &&
                                    <div className="news-card__avatar of-cover">
                                        <DamImage image={author.imageUrl} />
                                    </div>
                                }
                                <span className="news-card__author-name">{author.name}</span>
                            </div>
                        }
                        {
                            article.readTimeMinutes > 0 &&
                            <div className="news-card__reading-time">{article.readTimeMinutes} min read</div>
                        }
                        {
                            publishDate &&
                            <time className="news-card__date news-card__date--bottom" dateTime={publishDate}>
                                {format(parseISO(publishDate), 'd MMM yyyy')}
                            </time>
                        }
                        <div className="news-card__member-logo news-card__member-logo--bottom of-contain">
                            <DamImage image={'c5fcf2d0-d489-4fa2-b8b4-1f267e871e29.png'} />
                        </div>
                    </div>
                </div>
            </a>
        );
    }
};

NewsCard.defaultProps = {
    childCss: '',
    bemList: [],
};

export default NewsCard;
