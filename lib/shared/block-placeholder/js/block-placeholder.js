import React from 'react';
import PropTypes from 'prop-types';
import { BlockSpinner } from 'shared/block-spinner';

const BlockPlaceholder = ({ children, withSpinner = true, minHeight = 300 }) => {
    return (
        <div className="block-placeholder" style={{ minHeight }}>
            {withSpinner && <BlockSpinner />}
            {children}
        </div>
    );
};

BlockPlaceholder.propTypes = {
    children: PropTypes.node,
    minHeight: PropTypes.number
};

export default BlockPlaceholder;
