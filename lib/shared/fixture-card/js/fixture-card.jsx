import React from 'react';
import get from 'lodash/get';
import { format, parseISO } from 'date-fns';
import { deviceObserver } from '@deleteagency/device-observer';
import {
    generateBemCss,
    getTeamConfigPropById,
    getBemByStatus,
    isMatchStatusPre,
} from 'project/helpers';
import { app, matchStatuses } from 'project/general';
import { getThemeByTeamId } from 'project/themes';
import { gtmService } from 'project/services';
import { scrollObserver } from 'project/scroll-observer';
import { DamImage } from 'shared/dam-image';
import { GameStatus } from 'shared/game-status';
import { GameCardScore } from 'shared/game-card-score';
import { GameBroadcasters } from 'components/game-broadcasters';
import { promoTypes } from 'components/partner-heading';
import { TeamTitle } from 'components/team-title';

const ANIMATION_DURATION = 1200;

class FixtureCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bemList: [],
        };
        this.rootRef = React.createRef();
        this.animationTimeoutId = null;
        this.afterAnimationTimeoutId = null;
        this.unsubscribeScrollObserver = () => {};
    }

    componentDidMount() {
        if (deviceObserver.is('>=', 'desktop')) {
            // fallback
            if (!('IntersectionObserver' in window)) {
                this.animate();
            }

            if (this.rootRef.current) {
                this.unsubscribeScrollObserver = scrollObserver.subscribe(
                    this.rootRef.current,
                    () => {
                        this.animate();
                        this.unsubscribeScrollObserver();
                    }
                );
            }
        } else {
            this.afterAnimation();
        }
    }

    componentWillUnmount() {
        this.unsubscribeScrollObserver();
        clearTimeout(this.animationTimeoutId);
        clearTimeout(this.afterAnimationTimeoutId);
    }

    getRootRef() {
        if (this.rootRef.current) {
            return this.rootRef.current;
        }

        return null;
    }

    animate() {
        // delay for nice view
        this.animationTimeoutId = setTimeout(() => {
            this.setState({ bemList: ['intro-animation'] });

            this.afterAnimationTimeoutId = setTimeout(() => {
                this.afterAnimation();
            }, ANIMATION_DURATION);
        }, 200);
    }

    afterAnimation() {
        this.setState({
            bemList: ['animation-complete'],
        });
    }

    getTargetUrl() {
        const { homeTeam, awayTeam } = this.props.match;

        if (homeTeam.id === 7 || awayTeam.id === 7) return null;

        return this.props.match.url;
    }

    getPromoTypes() {
        const { match } = this.props;
        const isLive = match.status !== matchStatuses.POST &&
            !isMatchStatusPre(match.status);
        if (isLive) {
            return [
                promoTypes.MATCH_CENTRE_PRIMARY_BROADCASTER_DARK,
                promoTypes.MATCH_CENTRE_SECONDARY_BROADCASTER_DARK
            ];
        }

        return [
            promoTypes.MATCH_CENTRE_PRIMARY_BROADCASTER_COLOUR,
            promoTypes.MATCH_CENTRE_SECONDARY_BROADCASTER_COLOUR
        ];
    }

    renderDate() {
        const { isNextGame, isFiltered } = this.props;
        const { status, date } = this.props.match;

        switch (status) {
            case matchStatuses.POST:
                if (!isFiltered) return null;

                return (
                    <time className="fixture-card__date">
                        {`${format(parseISO(date), 'EEE dd MMM · HH:mm')}`}
                    </time>
                );
            case matchStatuses.PRE:
            case matchStatuses.POSTPONED:
            case matchStatuses.ABANDONED:
                if (isFiltered || isNextGame) {
                    return <time className="fixture-card__date">
                        {format(parseISO(date), 'EEE dd MMM · HH:mm')}
                    </time>;
                }
                return (
                    <time className="fixture-card__date">{format(parseISO(date), 'HH:mm')}</time>
                );
            default:
                return null;
        }
    }

    renderStatus() {
        const { status } = this.props.match;

        if (isMatchStatusPre(status)) return null;

        if (status === matchStatuses.POST) {
            return (
                <div className="fixture-card__fulltime">
                    <GameStatus bemList={['base', 'dark']}>full time</GameStatus>
                </div>
            );
        }

        return (
            <div className="fixture-card__live">
                <GameStatus bemList={['live']}>live</GameStatus>
            </div>
        );
    }

    renderInfo(team, bemList) {
        if (!team.conference) return null;

        return (
            <div className={generateBemCss('fixture-card__info', bemList)}>
                <span className="fixture-card__conference">({team.conference})</span>
                <span className="fixture-card__position">{team.positionFormatted}</span>
            </div>
        );
    }

    onClick = () => {
        const { status, homeTeam, awayTeam } = this.props.match;

        gtmService.push(
            app.state.siteSection,
            'Click',
            `${status} > ${getTeamConfigPropById(homeTeam.id, 'name')} v ${getTeamConfigPropById(awayTeam.id, 'name')}`
        );
    };

    getLogoProp() {
        const { match } = this.props;

        if (match.status !== matchStatuses.POST &&
            !isMatchStatusPre(match.status)) {
            return 'darkLogo';
        }

        return 'lightLogo';
    }

    render() {
        if (!this.props.match) return null;

        const { match } = this.props;
        const { homeTeam, awayTeam, venue } = match;
        const homeThemeCss = getThemeByTeamId(homeTeam.id);
        const awayThemeCss = getThemeByTeamId(awayTeam.id);
        const logoProp = this.getLogoProp();
        const url = this.getTargetUrl();
        const currentSeason = get(app, 'state.currentSeason');
        const bemCss = generateBemCss('fixture-card', [getBemByStatus(match.status), ...this.state.bemList]);

        return (
            <a className={bemCss} href={url} ref={this.rootRef} onClick={this.onClick}>
                <div className="fixture-card__inner">
                    <div className={generateBemCss('fixture-card__animline', ['top'])}>
                        <div className={`fixture-card__animline-home ${homeThemeCss}`}></div>
                        <div className={`fixture-card__animline-away ${awayThemeCss}`}></div>
                    </div>
                    <div className={`fixture-card__bg fixture-card__bg--home ${homeThemeCss}`}>
                        <DamImage
                            childCss={'fixture-card__bg-logo'}
                            image={getTeamConfigPropById(homeTeam.id, 'bgLogo')} />
                    </div>
                    <div className="fixture-card__content">
                        <div className="fixture-card__grid">
                            <div className="fixture-card__mid">
                                <div className="fixture-card__header">
                                    <DamImage
                                        childCss={'fixture-card__mobile-logo'}
                                        image={getTeamConfigPropById(homeTeam.id, logoProp)}
                                    />
                                    <div className="fixture-card__top">
                                        {this.renderDate()}
                                        {this.renderStatus()}
                                        {
                                            match.isPlayoff &&
                                            <div className="fixture-card__status">
                                                <div className="fixture-card__playoff">{match.playoffLabel}</div>
                                            </div>
                                        }
                                        {
                                            (
                                                match.status === matchStatuses.POSTPONED ||
                                                match.status === matchStatuses.ABANDONED
                                            ) &&
                                            <div className="fixture-card__status">
                                                <GameStatus bemList={['base', 'dark']}>{match.status}</GameStatus>
                                            </div>
                                        }
                                    </div>
                                    <DamImage
                                        childCss={'fixture-card__mobile-logo'}
                                        image={getTeamConfigPropById(awayTeam.id, logoProp)}
                                    />
                                </div>
                                <div className="fixture-card__teams">
                                    <div className={`fixture-card__side ${homeThemeCss}`}>
                                        <DamImage
                                            childCss={'fixture-card__desktop-logo'}
                                            image={getTeamConfigPropById(homeTeam.id, logoProp)} />
                                        {this.renderInfo(homeTeam, ['desktop'])}
                                    </div>
                                    <div className={`fixture-card__team fixture-card__team--home ${homeThemeCss}`}>
                                        <div className="fixture-card__score-wrap">
                                            <GameCardScore childCss={'fixture-card__score'} bemList={['dark']}>
                                                {homeTeam.score}
                                            </GameCardScore>
                                        </div>
                                        <div className="fixture-card__title">
                                            <TeamTitle
                                                name={getTeamConfigPropById(homeTeam.id, 'name') || homeTeam.name} />
                                        </div>
                                        {this.renderInfo(homeTeam, ['mobile'])}
                                    </div>
                                    <div className="fixture-card__versus">vs</div>
                                    <div className={`fixture-card__team fixture-card__team--away ${awayThemeCss}`}>
                                        <div className="fixture-card__score-wrap">
                                            <GameCardScore childCss={'fixture-card__score'} bemList={['dark']}>
                                                {awayTeam.score}
                                            </GameCardScore>
                                        </div>
                                        <div className="fixture-card__title">
                                            <TeamTitle
                                                name={getTeamConfigPropById(awayTeam.id, 'name') || awayTeam.name} />
                                        </div>
                                        {this.renderInfo(awayTeam, ['away', 'mobile'])}
                                    </div>
                                    <div className={`fixture-card__side ${awayThemeCss}`}>
                                        <DamImage
                                            childCss={'fixture-card__desktop-logo'}
                                            image={getTeamConfigPropById(awayTeam.id, logoProp)} />
                                        {this.renderInfo(awayTeam, ['away', 'desktop'])}
                                    </div>
                                </div>
                                <div className="fixture-card__place">
                                    <div>{venue && venue.name}</div>
                                    {
                                        venue?.homeGroundLocation &&
                                        <>
                                            <span className="fixture-card__separator">·</span>
                                            <div>{venue.homeGroundLocation}</div>
                                        </>
                                    }
                                </div>
                                {
                                    match.season === currentSeason &&
                                    <GameBroadcasters
                                        childCss={'fixture-card__broadcasters'}
                                        broadcasters={match.broadcasters}
                                        promoTypes={this.getPromoTypes()}
                                    />
                                }
                            </div>
                        </div>
                    </div>
                    <div className={`fixture-card__bg fixture-card__bg--away ${awayThemeCss}`}>
                        <DamImage
                            childCss={'fixture-card__bg-logo'}
                            image={getTeamConfigPropById(awayTeam.id, 'bgLogo')} />
                    </div>
                    <div className={generateBemCss('fixture-card__animline', ['bottom'])}>
                        <div className={`fixture-card__animline-home ${homeThemeCss}`}></div>
                        <div className={`fixture-card__animline-away ${awayThemeCss}`}></div>
                    </div>
                </div>
            </a>
        );
    }
}

export default FixtureCard;
