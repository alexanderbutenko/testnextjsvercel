import React from 'react';
import ReactDOM from 'react-dom';
import { BaseComponent } from 'project/general';
import Banner from './banner';

class BannerComponent extends BaseComponent {
    static getNamespace() {
        return 'banner';
    }

    static getRequiredRefs() {
        return ['data'];
    }

    onInit() {
        if (!this.initData) {
            throw new Error('Missed initial banner data');
        }

        ReactDOM.render(
            <Banner {...this.initData} />,
            this.element
        );
    }

    onDestroy() {
        ReactDOM.unmountComponentAtNode(this.element);
    }
}

export default BannerComponent;
