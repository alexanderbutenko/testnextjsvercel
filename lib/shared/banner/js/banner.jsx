import React from 'react';
import { PropTypes } from 'prop-types';
import generate from 'nanoid/generate';
import { bannerService } from 'project/services';

class Banner extends React.Component {
    constructor(props) {
        super(props);
        this.gptAdSlot = null;
        this.targetId = generate('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', 10);
        this.state = {
            hasPlacement: false,
        };
        this.timeoutId = null;
    }

    componentDidMount() {
        window.googletag = window.googletag || { cmd: [] };

        this.loadScript();

        const { sectionId, groupId, placementId } = this.props;

        bannerService.getPlacement(sectionId, groupId, placementId)
            .then((placement) => {
                this.setState({ hasPlacement: true });
                this.initBanner(placement);
            })
            .catch(() => {
                this.setState({ hasPlacement: false });
            });
    }

    componentWillUnmount() {
        this.destroyBanner();
        clearTimeout(this.timeoutId);
    }

    loadScript = () => {
        if (window.googletag && window.googletag.apiReady) return;

        const script = document.createElement('script');
        script.async = true;
        script.src = 'https://securepubads.g.doubleclick.net/tag/js/gpt.js';
        script.onerror = () => {
            if (this.props.onBannerCheck) this.props.onBannerCheck(false);
        };
        document.body.appendChild(script);
    };

    initBanner(placement) {
        if (!placement) return;

        const googletag = window.googletag;
        const sizes = placement.sizes || [[320, 100], [320, 250], [728, 90], [970, 250]];

        googletag.cmd.push(() => {
            this.gptAdSlot = googletag
                .defineSlot(
                    placement.slot,
                    sizes,
                    this.targetId
                )
                .addService(googletag.pubads());

            if (this.gptAdSlot) {
                this.gptAdSlot.defineSizeMapping(this.getMapping())
                    .addService(googletag.pubads());
            }

            googletag.pubads().enableSingleRequest();
            // comment this line for debug
            googletag.pubads().collapseEmptyDivs(true);
            googletag.enableServices();

            if (this.props.onBannerCheck) {
                const { onBannerCheck } = this.props;
                const slotName = placement.slot;

                googletag.pubads().addEventListener('slotRenderEnded', (event) => {
                    if (slotName === event.slot.getSlotId().getName()) {
                        onBannerCheck(!event.isEmpty);
                    }
                });
            }

            googletag.display(this.targetId);
        });
    }

    destroyBanner() {
        if (window.googletag && typeof window.googletag.destroySlots === 'function') {
            window.googletag.destroySlots(this.gptAdSlot);
        }
    }

    getMapping() {
        const { getMappting } = this.props;

        if (typeof getMappting === 'function') return getMappting();

        return window.googletag.sizeMapping()
            .addSize([1024, 0], [[970, 250], [728, 90]])
            .addSize([768, 0], [728, 90])
            .addSize([0, 0], [[320, 100], [320, 50]])
            .build();
    }

    render() {
        if (!this.state.hasPlacement) return null;

        return <div
            className="banner"
            id={this.targetId}
            onClick={this.onClickRoot}>
        </div>;
    }
}

Banner.defaultProps = {
    bemList: [],
};

Banner.propTypes = {
    sectionId: PropTypes.string,
    groupId: PropTypes.string,
    placementId: PropTypes.string,
    onBannerCheck: PropTypes.func
};

export default Banner;
