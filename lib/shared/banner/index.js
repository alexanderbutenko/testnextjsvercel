// import './scss/index.scss';
import { dcFactory } from '@deleteagency/dc';
import BanerComponent from './js/banner.component.js';
import Banner from './js/banner';

dcFactory.register(BanerComponent);

export {
    Banner,
};
