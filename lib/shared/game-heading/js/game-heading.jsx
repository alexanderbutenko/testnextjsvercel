import React from 'react';
import { generateBemCss } from '@/lib/project/helpers';

const GameHeading = ({ childCss, bemList, title }) => {
    const rootCss = generateBemCss('game-heading', bemList);

    return (
        <div className={`${childCss} ${rootCss}`}>
            <h2 className={'game-heading__title heading'}>{title}</h2>
        </div>
    );
};

GameHeading.defaultProps = {
    childCss: '',
};

export default GameHeading;
