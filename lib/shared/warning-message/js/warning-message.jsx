import React from 'react';

const WarningMessage = ({ title }) => {
    if (!title) return null;

    return (
        <div className="warning-message">
            <div className="warning-message__icon">!</div>
            <p className="warning-message__title">{title}</p>
        </div>
    );
};

export default WarningMessage;
