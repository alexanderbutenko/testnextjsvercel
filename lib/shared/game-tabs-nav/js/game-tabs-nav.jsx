import React from 'react';
import { compile } from 'path-to-regexp';
import { ArrayHelper } from 'foundation/helpers';
import { TabsNav } from 'components/tabs-nav';

const GameTabsNav = ({ tabs, filters }) => {
    if (ArrayHelper.isEmpty(tabs) || !filters) return null;

    const newTabs = tabs.map((tab) => {
        const compilePath = compile(tab.urlPattern);

        let pathString;

        try {
            pathString = compilePath(filters);
        } catch (error) {
            console.log(error);
            return tab;
        }

        return {
            ...tab,
            url: pathString
        };
    });


    return <TabsNav tabs={newTabs} />;
};

export default GameTabsNav;
