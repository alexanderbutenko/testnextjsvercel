import { Observable } from '@/lib/foundation/observable';
import { BaseComponent } from '@/lib/project/general';

class TeamSelectorComponent extends BaseComponent {
    constructor(...args) {
        super(...args);
        this.state = {
            selectedTeamId: null,
            items: {},
            onSelectObservable: new Observable(),
        };
    }

    static getNamespace() {
        return 'team-selector';
    }

    static getRequiredRefs() {
        return ['items', 'inputs'];
    }

    onInit() {
        if (!Array.isArray(this.refs.inputs)) {
            throw new Error('You must provide inputs as Array');
        }

        if (Array.isArray(this.refs.items)) {
            this.refs.items.forEach((itemNode) => {
                const teamId = itemNode.getAttribute('data-team-id');
                this.state.items[teamId] = itemNode;
            });
        }

        this.refs.inputs.forEach((inputNode) => {
            this.addListener(inputNode, 'click', this.onClickTeam);

            if (inputNode.checked) {
                this.state.selectedTeamId = inputNode.value;
            }
        });

        this._update();
    }

    onDestroy() {
        this._reset();
    }

    onClickTeam = (e) => {
        this.state.selectedTeamId = e.target.value;
        this._update();
        this.state.onSelectObservable.notify();
    };

    subscribeOnSelectTeam = (cb) => {
        return this.state.onSelectObservable.subscribe(cb);
    };

    reset = () => {
        this._reset();
    };

    _update() {
        const { selectedTeamId, items } = this.state;

        if (selectedTeamId === null) return;

        this.refs.inputs.forEach((inputNode) => {
            if (inputNode.value !== selectedTeamId) {
                if (items[inputNode.value]) {
                    items[inputNode.value].classList.add('is-disabled');
                }
            } else if (items[inputNode.value]) {
                items[inputNode.value].classList.remove('is-disabled');
            }
        });
    }

    _reset() {
        if (Array.isArray(this.refs.items)) {
            this.refs.items.forEach((itemNode) => {
                itemNode.classList.remove('is-disabled');
                itemNode.checked = false;
            });
        }
    }
}

export default TeamSelectorComponent;
