import React from 'react';
import PropTypes from 'prop-types';
import { generateBemCss } from '@/lib/project/helpers';

const CarouselDots = ({ fakeDots, childCss, bemList }) => {
    if (!fakeDots) return null;

    const rootCss = generateBemCss('carousel-dots', bemList);
    const firstDot = fakeDots.isFirst ? 'is-active' : '';
    const middleDot = (!fakeDots.isFirst && !fakeDots.isLast) ? 'is-active' : '';
    const lastDot = fakeDots.isLast ? 'is-active' : '';

    return (
        <div className={`${childCss} ${rootCss}`}>
            <div className={`carousel-dots__dot ${firstDot}`}></div>
            <div className={`carousel-dots__dot ${middleDot}`}></div>
            <div className={`carousel-dots__dot ${lastDot}`}></div>
        </div>
    );
};

CarouselDots.defaultProps = {
    childCss: '',
};

CarouselDots.propTypes = {
    fakeDots: PropTypes.shape({
        isFirst: PropTypes.bool,
        isLast: PropTypes.bool,
    })
};

export default CarouselDots;
