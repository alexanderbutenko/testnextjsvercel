import React from 'react';
import PropTypes from 'prop-types';
import Choices from 'choices.js';
import { deviceObserver } from '@deleteagency/device-observer';
import { generateBemCss } from 'project/helpers';

class LiveEventSelect extends React.Component {
    constructor(props) {
        super(props);
        this.bemName = props.bemName;
        this.bemList = props.bemList;
        this.choicesInstance = null;
        this.selectRef = React.createRef();
        this.rootCss = generateBemCss(this.bemName, this.bemList);
    }

    componentDidMount() {
        this.choicesInstance = new Choices(this.selectRef.current, {
            choices: [],
            searchEnabled: false,
            shouldSort: false,
            itemSelectText: '',
            classNames: {
                containerOuter: `${this.bemName}__outer`,
                containerInner: `${this.bemName}__inner`,
                list: `${this.bemName}__list`,
                //listDropdown: `${this.bemName}__list--dropdown scrollbar`,
                listDropdown: `${this.bemName}__list--dropdown`,
                listSingle: `${this.bemName}__list--single`,
                item: `${this.bemName}__item`,
                itemSelectable: `${this.bemName}__item--selectable`,
                itemDisabled: `${this.bemName}__item--disabled`,
                itemChoice: `${this.bemName}__item--choice`
            }
        });

        this.updateChoices();
        this.unsubscribeOnChange = deviceObserver.subscribeOnChange(this.onDeviceTypeChanged);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.choices !== this.props.choices || this.props.shouldBeReset) {
            this.choicesInstance.setChoices(this.props.choices, 'value', 'label', true);
            this.updateChoices();
        }
    }

    componentWillUnmount() {
        this.unsubscribeOnChange();
        this.choicesInstance.destroy();
        this.choicesInstance = null;
    }

    onDeviceTypeChanged = () => {
        this.updateChoices();
    };

    getOptionValue = () => {
        const value = this.choicesInstance.getValue(true);

        if (typeof this.props.onChange === 'function') {
            this.props.onChange(value);
        }
    };

    updateChoices() {
        const { choices } = this.props;

        if (!Array.isArray(choices)) return;

        const selectedValue = this.choicesInstance.getValue(true);
        const newChoices = choices.map((choice) => {
            const mobileChoice = { ...choice };

            if (deviceObserver.is('<=', 'tablet')) {
                mobileChoice.label = choice.mobileLabel;
            }

            if (selectedValue) {
                mobileChoice.selected = choice.value === selectedValue;
            }

            return mobileChoice;
        });

        this.choicesInstance.setChoices(newChoices, 'value', 'label', true);
    }

    render() {
        return (
            <div className={this.rootCss}>
                <select
                    className="dropdown__native"
                    ref={this.selectRef}
                    onChange={this.getOptionValue}
                >
                </select>
            </div>
        );
    }
}

LiveEventSelect.defaultProps = {
    bemName: 'dropdown',
    bemList: [],
    choices: [],
    shouldBeReset: false,
};

LiveEventSelect.propTypes = {
    choices: PropTypes.array,
    theme: PropTypes.object,
    onChange: PropTypes.func
};

export default LiveEventSelect;
