import cn from 'classnames/bind';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { generateBemCss } from '@/lib/project/helpers';
import styles from '../scss/block-spinner.module.scss';

const BlockSpinner = ({ bemList, hasOverlay }) => {
    const cx = cn.bind(styles);
    const rootCss = generateBemCss(cx('block-spinner'), bemList);
    const stateCss = classNames({
        'has-overlay': hasOverlay
    });

    return (
        <div className={`${rootCss} ${stateCss}`}></div>
    );
};

BlockSpinner.propTypes = {
    hasOverlay: PropTypes.bool
};

export default BlockSpinner;
