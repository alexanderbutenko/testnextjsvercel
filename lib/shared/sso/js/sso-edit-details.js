import { userProfile } from '@/lib/project/user-profile';
import BaseSso from './base-sso';

class SsoEditDetails extends BaseSso {
    onSubmit = () => {
        const formData = this.form._getFormDataObject();

        userProfile
            .updateDetails(this.form._getFormDataObject())
            .then(() => {
                if (formData.oldPassword && formData.password) {
                    userProfile
                        .changePassword(formData)
                        .then(() => {
                            this._reset();
                            this.close();
                        })
                        .catch((error) => {
                            this._renderServerErrors(error);
                        });
                } else {
                    this._reset();
                    this.close();
                }
            })
            .catch((error) => {
                this._renderServerErrors(error);
            });

        return false;
    };

    _getModalOptions() {
        return ({
            bemList: ['tiny'],
            autoCloseOnClickOutside: false,
        });
    }

    _getFormModel() {
        return userProfile.getProfile().details;
    }
}

export default SsoEditDetails;
