export const formTypes = Object.freeze({
    SIGN_IN: 'signIn',
    REGISTER: 'register',
    FORGOTTEN_PASSWORD: 'forgottenPassword',
    CHANGE_TEAM: 'editTeam',
    EDIT_DETAILS: 'editDetails',
    EDIT_PREFERENCES: 'editPreferences',
});
