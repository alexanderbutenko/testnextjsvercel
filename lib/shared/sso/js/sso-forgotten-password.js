import { gtmService } from '@/lib/project/services';
import { userProfile } from '@/lib/project/user-profile';
// import { AccountResetComponent } from '@/components/account-reset';
import BaseSso from './base-sso';

class SsoForgottenPassword extends BaseSso {
    onSubmit = () => {
        const formData = this.form._getFormDataObject();

        userProfile
            .forgottenPassword(formData.email)
            .then(() => {
                this.accountResetInstance.showSuccessPanel();
                gtmService.push('Member', 'Reset Password', 'Send Email');
            })
            .catch((error) => {
                this._renderServerErrors(error);
            });

        return false;
    };

    _afterOpen() {
        if (this.accountResetInstance) {
            this.accountResetInstance.destroy();
        }

        const rootNode = this.modal.element.querySelector('[data-account-reset]');
        this.accountResetInstance = new AccountResetComponent(rootNode);
        this.accountResetInstance.init();
    }

    _getModalOptions() {
        return ({
            bemList: ['tiny'],
            autoCloseOnClickOutside: false,
        });
    }
}

export default SsoForgottenPassword;
