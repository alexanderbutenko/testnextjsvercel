import { Form } from '@deleteagency/forms';
import { baseApi } from '@/lib/project/api';
import get from 'lodash/get';

const INPUTS_SELECTOR = 'input, textarea, select';
const INPUTS_SELECTOR_EXCLUDED = 'input[type=button], input[type=submit], input[type=reset], input[type=hidden]';

const BIND_ATTRIBUTE = 'data-user-bind';
const BIND_MULTIPLE_ATTRIBUTE = 'data-user-bind-multiple';

class BaseSsoForm extends Form {
    constructor(el, options, hooks) {
        const modalOptions = {
            ...options.modalOptions,
            submitElement: el.querySelector('button[type="submit"]'),
            errorsSummaryElement: el.querySelector('[data-errors-summary]'),
            parsley: {
                errorClass: 'is-invalid',
                successClass: 'is-valid',
                errorsWrapper: '<ul class="form-errors" aria-live="assertive"></ul>',
                errorTemplate: '<li class="form-error__li"></li>',
            },
            axiosInstance: baseApi
        };
        super(el, modalOptions);
        this.model = options.initData || null;
        this.hooks = hooks;

        if (this.model) {
            this._applyBindings();
        }
    }

    destroy() {
        this.element.reset();
        super.destroy();
    }

    onSubmit() {
        if (this.hooks.onSubmit) {
            this.hooks.onSubmit();
            return false;
        }

        return true;
    }

    _getModel() {
        return this.model;
    }

    _getModelValue(prop, defaultValue) {
        return get(this._getModel(), prop, defaultValue);
    }

    _applyBindings() {
        const bindElements = [...this.element.querySelectorAll(`[${BIND_ATTRIBUTE}]`)];
        const inputElements = [];
        const textElements = [];

        bindElements.forEach((element) => {
            if (element.matches(INPUTS_SELECTOR) && !element.matches(INPUTS_SELECTOR_EXCLUDED)) {
                inputElements.push(element);
            } else {
                textElements.push(element);
            }
        });

        this._applyInputsBindings(inputElements);
        this._applyTextBindings(textElements);
    }

    _applyInputsBindings(elements) {
        elements.forEach((el) => {
            const prop = el.getAttribute(BIND_ATTRIBUTE);
            this._applyInputValue(el, this._getModelValue(prop));
        });
    }

    _applyInputValue(input, value) {
        switch (input.type) {
            case 'checkbox':
                this._applyCheckboxValue(input, value);
                break;
            case 'radio':
                this._applyRadioValue(input, value);
                break;
            case 'select-multiple':
                break;
            default:
                input.value = value || '';
        }
        //input.dispatchEvent(createEvent('change'));
    }

    _applyCheckboxValue(input, value) {
        if (input.hasAttribute(BIND_MULTIPLE_ATTRIBUTE)) {
            const modelValue = value || [];
            const inputValue = input.value;
            input.checked = modelValue.includes(inputValue);
        } else {
            input.checked = value || false;
        }
    }

    _applyTextBindings(elements) {
        elements.forEach((el) => {
            const prop = el.getAttribute(BIND_ATTRIBUTE);
            if (prop) {
                el.innerHTML = this.getModelValue(prop, '');
            }
        });
    }

    _applyRadioValue(input, value) {
        if (value && input.value === value.toString()) {
            input.checked = true;
        }
    }
}

export default BaseSsoForm;


window.Parsley.addValidator('requiredIfNonEmptyRef', {
    requirementType: 'string',
    validateString: (value, requirement) => {
        const refNode = document.querySelector(requirement);

        return !(refNode && refNode.value && !value);
    },
    priority: 512,
    messages: {
        en: 'This value is required',
    }
});
