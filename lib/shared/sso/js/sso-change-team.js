import { userProfile } from '@/lib/project/user-profile';
import { TeamSelectorComponent } from '@/lib/shared/team-selector';
import BaseSso from './base-sso';

class SsoChangeTeam extends BaseSso {
    onSubmit = () => {
        userProfile
            .updateFavouriteTeam(this.form._getFormDataObject())
            .then(() => {
                this._reset();
                this.close();
            })
            .catch((error) => {
                this._renderServerErrors(error);
            });

        return false;
    };

    _afterOpen() {
        if (this.teamSelector) {
            this.teamSelector.destroy();
        }

        const teamSelectorNode = this.modal.element.querySelector('[data-team-selector]');
        this.teamSelector = new TeamSelectorComponent(teamSelectorNode);
        this.teamSelector.init();
    }

    _getFormModel() {
        return userProfile.getProfile().team;
    }

    _getModalOptions() {
        return ({
            autoCloseOnClickOutside: false,
        });
    }
}

export default SsoChangeTeam;
