import get from 'lodash/get';
import { baseApi, isHtmlContentType } from '@/lib/project/api';
// import { Modal } from '@/lib/shared/modal';
// import { pageSpinner } from '@/lib/shared/page-spinner';
import BaseSsoForm from './base-sso-form';

class BaseSso {
    constructor(url) {
        if (!url) {
            throw new Error('Invalid enpoint url');
        }

        this.state = {
            url,
            onSuccessSubscribers: [],
        };
        this.modal = null;
        this.isOpenning = false;
        this.form = null;
    }

    open = () => {
        if (this.isOpenning) return;

        this.isOpenning = true;

        this.getModal().then((modal) => {
            try {
                modal.open();
                this.isOpenning = false;
                const formNode = modal.element.querySelector('form');

                if (this.form) {
                    this.form.destroy();
                }

                this.form = new BaseSsoForm(
                    formNode,
                    { initData: this._getFormModel() },
                    { onSubmit: this.onSubmit, }
                );

                this._afterOpen();
            } catch (error) {
                console.log(error);
            }
        }).catch((error) => {
            this.isOpenning = false;
            console.log(error);
        });
    };

    close = () => {
        this.getModal().then((modal) => {
            modal.close();
        });
    };

    getModal() {
        if (this.modal !== null) return Promise.resolve(this.modal);

        return this.getTarget()
            .then((data) => {
                try {
                    // this.modal = Modal.create(data, this._getModalOptions());
                    return this.modal;
                } catch (error) {
                    return Promise.reject(error);
                }
            }).catch((error) => {
                // pageSpinner.hide();
                return Promise.reject(error);
            });
    }

    getTarget() {
        const targetUrl = this.state.url + '?modal=true';

        // pageSpinner.show();

        return baseApi.get(targetUrl)
            .then((response) => {
                if (isHtmlContentType(response)) {
                    return Promise.resolve(response.data);
                }

                //return Promise.reject('Unknown result from async data');
                return Promise.resolve(response.data.html);
            }).catch((error) => {
                return Promise.reject(error);
            })
            .finally(() => {
                // pageSpinner.hide();
            });
    }

    _renderServerErrors(error) {
        const message = get(error, 'response.data.message') || 'Sorry, unknown error';
        this.form._applyErrorsSummary([message]);
    }

    _reset() {
        this.form._parsleyForm.reset();
    }

    _afterOpen() {
    }

    _getModalOptions() {
    }

    _getFormModel() {
    }
}

export default BaseSso;
