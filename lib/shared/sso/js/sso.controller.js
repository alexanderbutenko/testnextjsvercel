import { app } from '@/lib/project/general';
import SsoLogin from './sso-login';
import SsoRegister from './sso-register';
import SsoForgottenPassword from './sso-forgotten-password';
import SsoChangeTeam from './sso-change-team';
import SsoEditDetails from './sso-edit-details';
import SsoEditPreferences from './sso-edit-preferences';
import { formTypes } from './enums';

class SsoController {
    constructor() {
        const { sso } = app.state;

        if (!sso || !sso.formUrls) {
            throw new Error('You must provide sso.formUrls data');
        }

        this.loginForm = new SsoLogin(sso.formUrls[formTypes.SIGN_IN]);
        this.registerForm = new SsoRegister(sso.formUrls[formTypes.REGISTER]);
        this.forgottenPasswordForm = new SsoForgottenPassword(sso.formUrls[formTypes.FORGOTTEN_PASSWORD]);
        this.changeTeamForm = new SsoChangeTeam(sso.formUrls[formTypes.CHANGE_TEAM]);
        this.editDetailsForm = new SsoEditDetails(sso.formUrls[formTypes.EDIT_DETAILS]);
        this.editPreferencesForm = new SsoEditPreferences(sso.formUrls[formTypes.EDIT_PREFERENCES]);
    }

    show = (type) => {
        switch (type) {
            case formTypes.SIGN_IN:
                this.loginForm.open();
                break;
            case formTypes.REGISTER:
                this.registerForm.open();
                break;
            case formTypes.FORGOTTEN_PASSWORD:
                this.forgottenPasswordForm.open();
                break;
            case formTypes.CHANGE_TEAM:
                this.changeTeamForm.open();
                break;
            case formTypes.EDIT_DETAILS:
                this.editDetailsForm.open();
                break;
            case formTypes.EDIT_PREFERENCES:
                this.editPreferencesForm.open();
                break;
            default:
                console.log('Unknown type:', type);
        }
    };
}

const instance = new SsoController();
export default instance;
