// import { AccountRegisterComponent } from 'components/account-register';
import BaseSso from './base-sso';

class SsoRegister extends BaseSso {
    onSubmit = () => {
        return false;
    };

    onValidationSuccess = () => {
        this.close();
    };

    _afterOpen() {
        if (this.accountRegisterInstance) {
            this.accountRegisterInstance.destroy();
        }

        const accountNode = this.modal.element.querySelector('[data-account-register]');
        this.accountRegisterInstance = new AccountRegisterComponent(accountNode);
        this.accountRegisterInstance.init();
        this.accountRegisterInstance.subscribeOnValidationSuccess(this.onValidationSuccess);
    }

    _getModalOptions() {
        return ({
            bemList: ['light'],
            autoCloseOnClickOutside: false,
        });
    }
}

export default SsoRegister;
