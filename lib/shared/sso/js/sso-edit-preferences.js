import { userProfile } from '@/lib/project/user-profile';
import BaseSso from './base-sso';

class SsoEditPreferences extends BaseSso {
    onSubmit = () => {
        userProfile
            .updatePreferences(this.form._getFormDataObject())
            .then(() => {
                this._reset();
                this.close();
            })
            .catch((error) => {
                this._renderServerErrors(error);
            });
    };

    _getModalOptions() {
        return ({
            bemList: ['tiny'],
            autoCloseOnClickOutside: false,
        });
    }

    _getFormModel() {
        return userProfile.getProfile().preferences;
    }
}

export default SsoEditPreferences;
