import { gtmService } from '@/lib/project/services';
import { userProfile } from '@/lib/project/user-profile';
import BaseSso from './base-sso';

class SsoLogin extends BaseSso {
    onSubmit = () => {
        userProfile
            .login(this.form._getFormDataObject())
            .then(() => {
                this._reset();
                this.close();
                gtmService.push('Member', 'Sign In', 'Successful Sign In');

                // refresh the page to show restricted content
                if (localStorage.getItem("videoEntryID") == null && window.location.href.includes('/latest/')) {
                    window.location.reload();
                }
                if(localStorage.getItem("videoUrl") !== null && localStorage.getItem("videoUrl") != window.location.href){
                    window.location = localStorage.getItem("videoUrl");
                    localStorage.removeItem("videoUrl")
                }
                if (localStorage.getItem("videoEntryID") !== null) {
                    document.getElementsByClassName(`stream-amg-card ${localStorage.getItem("videoEntryID")}`)[0].click();
                    localStorage.removeItem("videoEntryID");
                }
            })
            .catch((error) => {
                this._renderServerErrors(error);
            });

        return false;
    };

    _getModalOptions() {
        return ({
            bemList: ['light'],
            autoCloseOnClickOutside: false,
        });
    }
}

export default SsoLogin;
