import React from 'react';
import { generateBemCss } from 'project/helpers';
import { DamImage } from 'shared/dam-image';

const LineupsHeading = ({ title, image, childCss, bemList }) => {
    const rootCss = generateBemCss('lineups-heading', bemList);

    return (
        <div className={`${childCss} ${rootCss}`}>
            <div className="lineups-heading__logo of-contain">
                <DamImage image={image} alt={`The Background Image of ${title} Group`} />
            </div>
            <h3 className="lineups-heading__title">
                <span className="lineups-heading__text">{title}</span>
            </h3>
        </div>
    );
};

LineupsHeading.defaultProps = {
    childCss: '',
};

export default LineupsHeading;
