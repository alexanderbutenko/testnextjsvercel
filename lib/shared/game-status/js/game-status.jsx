import React from 'react';
import { generateBemCss } from 'project/helpers';

const GameStatus = ({ children, childCss, bemList }) => {
    const rootCss = generateBemCss('game-status', bemList);

    return (
        <div className={`${childCss} ${rootCss}`}>{children}</div>
    );
};

GameStatus.defaultProps = {
    childCss: '',
};

export default GameStatus;
