import { pageLocker } from '@deleteagency/page-locker';

class PageSpinner {
    constructor() {
        this.isVisible = false;
        this.isInited = false;
        this.currentOptions = null;
        this.spinnerElement = null;
    }

    init() {
        this.spinnerElement = document.createElement('div');
        this.spinnerElement.classList.add('page-spinner');
        document.body.insertAdjacentElement('beforeend', this.spinnerElement);
        this.isInited = true;
    }

    show(options = { fade: false, blockContent: false }) {
        if (!this.isInited) {
            this.init();
        }

        if (!this.isVisible) {
            this.applyOptions(options);
            this.currentOptions = options;
            if (this.currentOptions.blockContent) {
                pageLocker.lock('page-spinner');
            }

            this.spinnerElement.classList.add('is-visible');
            this.isVisible = true;
        }
    }

    hide() {
        if (this.isVisible) {
            if (this.currentOptions.blockContent) {
                pageLocker.unlock('page-spinner');
            }
            this.spinnerElement.classList.remove('is-visible');
            this.isVisible = false;
        }
    }

    applyOptions(options) {
        if (options.fade) {
            this.spinnerElement.classList.add('has-fade');
        } else {
            this.spinnerElement.classList.remove('has-fade');
        }
    }
}

const instance = new PageSpinner();
export default instance;
