import React from 'react';
import { app, statsValueTypes } from 'project/general';
import { deviceObserver } from '@deleteagency/device-observer';
import { gtmService } from 'project/services';
import { getThemeByTeamId } from 'project/themes';
import {
    generateBemCss,
    getBemList,
    getPlayerImageOrDefault,
    getPlayerName,
    getTeamConfigPropById,
} from 'project/helpers';
import { scrollObserver } from 'project/scroll-observer';
import { DamImage } from 'shared/dam-image';

const ANIMATION_DURATION = 750;

class TeamLineupsCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bemList: [],
        };
        this.rootRef = React.createRef();
        this.animationTimeoutId = null;
        this.afterAnimationTimeoutId = null;
        this.unsubscribeScrollObserver = () => {};
    }

    componentDidMount() {
        if (deviceObserver.is('>=', 'desktop')) {
            // fallback
            if (!('IntersectionObserver' in window)) {
                this.animate();
            }

            if (this.rootRef.current) {
                this.unsubscribeScrollObserver = scrollObserver.subscribe(
                    this.rootRef.current,
                    () => {
                        this.animate();
                        this.unsubscribeScrollObserver();
                    }
                );
            }
        } else {
            this.afterAnimation();
        }
    }

    componentWillUnmount() {
        this.unsubscribeScrollObserver();
        clearTimeout(this.animationTimeoutId);
        clearTimeout(this.afterAnimationTimeoutId);
    }

    animate() {
        // delay for nice view
        this.animationTimeoutId = setTimeout(() => {
            this.setState({ bemList: ['intro-animation'] });
            this.afterAnimationTimeoutId = setTimeout(() => {
                this.afterAnimation();
            }, ANIMATION_DURATION);
        }, 200);
    }

    afterAnimation() {
        this.setState({
            bemList: ['animation-complete'],
        });
    }

    onClick = (player) => {
        gtmService.push(
            app.state.siteSection,
            'Select Player',
            `${player.firstName} ${player.lastName} > ${this.props.title} > ${getTeamConfigPropById(player.teamId, 'name')}`
        );
    };

    renderValue = (player) => {
        switch (player.type) {
            case statsValueTypes.MIN_MAX:
                return (
                    <>
                        <span>{player.value}</span>
                        <span className="team-lineups-card__additional">/ {player.maxValue}</span>
                    </>);
            case statsValueTypes.PERCENT:
                return (
                    <>
                        <span>{player.value}</span>
                        <span className="team-lineups-card__additional">%</span>
                    </>
                );
            default:
                return player.value;
        }
    };

    renderPlayer(player, bemModifier) {
        const { isRenderPlayerImage } = this.props;
        const cardCss = generateBemCss('team-lineups-card__player', [bemModifier]);
        const themeCss = getThemeByTeamId(player.teamId);

        return (
            <a
                className={`${cardCss} ${themeCss}`}
                href={player.url}
                onClick={() => this.onClick(player)}
                ref={this.rootRef}
            >
                {
                    isRenderPlayerImage &&
                    <div className="team-lineups-card__team of-contain">
                        <DamImage
                            image={getPlayerImageOrDefault(player.imageSrc)}
                            alt={`The Photo of ${player.firstName} ${player.lastName}`} />
                    </div>
                }
                <div className="team-lineups-card__player-info">
                    <div className="team-lineups-card__fullname">
                        <p className="team-lineups-card__firstname">{getPlayerName(player).firstName}</p>
                        <p className="team-lineups-card__lastname">{getPlayerName(player).lastName}</p>
                        {
                            player.rankFormatted &&
                            <span className="team-lineups-card__rank team-lineups-card__rank--desktop">
                                {player.rankFormatted}
                            </span>
                        }
                    </div>
                    {
                        player.type &&
                        <div className="team-lineups-card__stats">
                            <span className="team-lineups-card__value">
                                {this.renderValue(player)}
                            </span>
                        </div>
                    }
                    {
                        player.rankFormatted &&
                        <div className="team-lineups-card__rank">
                            {player.rankFormatted}
                        </div>
                    }
                </div>
            </a>
        );
    }

    render() {
        const {
            title,
            position,
            homePlayer,
            awayPlayer,
            bemList
        } = this.props;

        if (!homePlayer || !awayPlayer) return null;

        const rootCss = generateBemCss('team-lineups-card', getBemList(bemList, this.state.bemList));

        return (
            <div className={rootCss}>
                {this.renderPlayer(homePlayer, 'home')}
                <div className="team-lineups-card__center">
                    <h4 className="team-lineups-card__title">{title}</h4>
                    {position && <span className="team-lineups-card__position">{position}</span>}
                </div>
                {this.renderPlayer(awayPlayer, 'away')}
            </div>
        );
    }
}

TeamLineupsCard.defaultProps = {
    bemList: [],
};

export default TeamLineupsCard;
