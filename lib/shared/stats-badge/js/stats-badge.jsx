import React from 'react';
import { SpriteSvg } from 'project/sprite-svg';

const StatsBadge = ({ value }) => {
    return (
        <div className="stats-badge">
            <SpriteSvg iconName={'stats'} width={8} height={8} />
            <span>{value}</span>
        </div>
    );
};

export default StatsBadge;
