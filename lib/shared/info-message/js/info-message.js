import React from 'react';
import PropTypes from 'prop-types';
import { generateBemCss } from 'project/helpers';

const InfoMessage = ({ model, bemList }) => {
    const bemCss = generateBemCss('info-message', bemList);

    return (
        <div className={bemCss}>
            <div className="">{model.title}</div>
            <div className="">{model.body}</div>
        </div>
    );
};

InfoMessage.defaultProps = {
    model: {
        title: 'The data you are looking for is not available at this time',
        body: ''
    }
};

InfoMessage.propTypes = {
    model: PropTypes.shape({
        title: PropTypes.string,
        body: PropTypes.string,
    }),
    bemList: PropTypes.array,
};

export default InfoMessage;
