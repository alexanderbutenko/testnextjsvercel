import React from 'react';
import { Endpoints } from '@/lib/endpoints';
import { getSrcSetStreamAmg } from '@/lib/project/helpers';
import { sizes } from '@/lib/project/general';
import { LazyImage } from '@/lib/project/lazy-image';

const DamImage = ({ image, ...rest }) => {
    if (!image) return null;

    if (image.includes('streamamg')) {
        return <LazyImage image={getSrcSetStreamAmg(image)} {...rest} />;
    }

    if (image.includes('wp-content')) {
        return <LazyImage image={image} {...rest} />;
    }

    let path = `${Endpoints.IncrowdSports}${image}`;

    if (image.includes('http')) {
        path = image;
    }

    const srcString = [];

    const getDelimiter = () => {
        if (image.includes('?')) return '&';

        return '?';
    };

    sizes.forEach((size) => {
        srcString.push(`${path}${getDelimiter()}width=${size} ${size}w`);
    });

    return <LazyImage image={srcString.join(', ')} {...rest} />;
};

export default DamImage;
