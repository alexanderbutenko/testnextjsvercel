import { modalService } from '@deleteagency/modal';
// import { dcFactory } from '@deleteagency/dc';
import { pageLocker } from '@deleteagency/page-locker';
import { generateBemCss } from '@/lib/project/helpers';
// import { Overlay } from '@/components/overlay';

// const overlay = new Overlay('overlay', 'overlay');

modalService.init({
    onModalInit: (modal) => {
        // dcFactory.init(modal.element);
    },
    onModalDestroy: (modal) => {
        // dcFactory.destroy(modal.element);
    },
    onBeforeFirstModalOpen: () => {
        pageLocker.lock('modal');
        // overlay.show('modal');
    },
    onAfterLastModalClose: () => {
        pageLocker.unlock('modal');
        // overlay.hide('modal');
    },
    defaultModalTemplate: ({ bemList }) => {
        const rootCss = generateBemCss('modal', bemList);

        return (`
        <div data-modal class="${rootCss}" aria-hidden="true" role="dialog">
            <div class="modal__wrapper" data-modal-wrapper>
                <button class="modal__close" data-modal-close type="button" aria-label="close popup">
                    <span class="button-content">
                        <svg class="icon modal__close-icon modal__close-icon--cross" width="18" height="18" focusable="false">
                            <use xlink:href="#icon-cross"></use>
                        </svg>
                    </span>
                </button>
                <div class="modal__content" data-modal-content>
                </div>
                </div>
            </div>
        </div>
        `);
    }

});

export default modalService;
