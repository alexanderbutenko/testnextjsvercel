import React from 'react';
import { generateBemCss } from 'project/helpers';

const FormBadge = ({ result }) => {
    if (!result) return null;

    const rootCss = generateBemCss('form-badge', [result.toLowerCase()]);

    return (
        <div className={rootCss}>{result}</div>
    );
};

export default FormBadge;
