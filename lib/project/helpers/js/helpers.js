import { sizes } from '@/lib/project/general';

export const generateBemCss = (bemName, bemList) => {
    let bemCss = '';

    if (Array.isArray(bemList)) {
        bemCss = bemList
            .filter((bemModifier) => bemModifier)
            .map((bemModifier) => `${bemName}--${bemModifier}`).join(' ');
    }

    return bemCss ? `${bemName} ${bemCss}` : bemName;
};

export const generateBemCssModule = (bemName, bemList, styles) => {
    return generateBemCss(bemName, bemList)
        .split(' ')
        .map((css) => styles[css])
        .join(' ');
};

export const generateModuleName = (rootClass, styles, bemList) => {
    return bemList.map((css) => {
        const bemCss = (css.replace('-', ' ')
            .replace(/(^\w)|(\s\w)/g, (match) => match.toUpperCase())
            .replace(' ', ''));
        return styles[rootClass + bemCss];
    }).join(' ');
};

export const getBemList = (propsBemList, stateBemList) => {
    let bemList = [];

    if (Array.isArray(propsBemList)) {
        bemList = [...propsBemList];
    }

    if (Array.isArray(stateBemList)) {
        bemList = [...bemList, ...stateBemList];
    }

    return bemList;
};

export const getCategoryString = (string) => {
    if (!string) return '';

    return string.toLocaleLowerCase().replace(/(\s.\s|\s)/, '-');
};

// https://github.com/facebook/react/issues/5465
export const makeCancelable = (promise) => {
    let hasCanceled_ = false;

    const wrappedPromise = new Promise((resolve, reject) => {
        promise.then(
            (val) => hasCanceled_ ? reject({ isCanceled: true }) : resolve(val),
            (error) => hasCanceled_ ? reject({ isCanceled: true }) : reject(error)
        );
    });

    return {
        promise: wrappedPromise,
        cancel() {
            hasCanceled_ = true;
        },
    };
};

export const getScrollTop = () => {
    return document.documentElement.scrollTop || document.body.scrollTop;
};

export const setScrollTop = (value) => {
    if (typeof value !== 'number') return;

    document.body.scrollTop = value;
    document.documentElement.scrollTop = value;
};

export const getData = async (url) => {
    const response = await fetch(url);
    return response.json();
};

export const getSrcSetStreamAmg = (image) => {
    const regExp = image.match(/(width\/\d*\/height\/\d*)/g).toString();

    const getSizes = (size) => {
        return image.replace(regExp, `width/${size}`);
    };

    const srcString = [];

    sizes.forEach((size) => {
        srcString.push(`${getSizes(size)} ${size}w`);
    });

    return srcString.join(', ');
};
