import { compile } from 'path-to-regexp';
import { app, store } from '@/lib/project/general';
import { FetchService } from '@/lib/project/services';
import { receiveData } from '@/lib/ducks/feeds.reducer';

export const endpointNames = Object.freeze({
    COMPETITION_MATCHES: 'competitionMatches',
    MATCH_DETAILS: 'matchDetails',
    MATCH_STATS: 'matchStats',
    COMPETITION_TABLE: 'competitionTable',
    TEAM_STATS: 'teamStats',
    TEAMS_STATS: 'teamsStats',
    MEDIA_CENTRE: 'mediaCentre',
    LATEST_NEWS: 'latestNews',
    PLAYER_STATS: 'playerStats',
    PLAYERS_STATS: 'playersStats',
    TOP_PLAYERS_STATS: 'topPlayersStats',
    BLOG: 'matchBlogs',
    BLOG_BATCHES: 'liveBlogBatches',
    BANNERS: 'bannersData',
    FIXTURES_CAROUSEL: 'fixturesCarouselData',
});

class FeedApi {
    constructor() {
        this.state = {
            activeEndpoints: {}
        };
    }

    fetchMany(enpointName, params) {
        const url = this._getUrlByName(enpointName, params);
        const endpointConfig = this.getEndpointConfig(enpointName);

        if (!this.state.activeEndpoints[url]) {
            const fetchService = new FetchService({
                config: {
                    url,
                },
                updateInSeconds: endpointConfig.updateInSeconds,
                successCallback: (data) => store.dispatch(receiveData(url, data)),
            });

            fetchService.many();
            this.state.activeEndpoints[url] = {
                subscribers: 0,
                fetchService
            };
        }

        this.state.activeEndpoints[url].subscribers += 1;

        return () => {
            this.state.activeEndpoints[url].subscribers -= 1;

            if (this.state.activeEndpoints[url].subscribers === 0) {
                this.state.activeEndpoints[url].fetchService.destroy();
                delete this.state.activeEndpoints[url];
            }
        };
    }

    _getUrlByName(enpointName, params) {
        const config = this.getEndpointConfig(enpointName);
        const toPath = compile(config.path);
        return toPath(params);
    }

    getEndpointConfig(endointName) {
        const configs = app.state.feedConfig;
        const foundConfig = configs.endpoints.find((endpoint) => endpoint.name === endointName);

        if (!foundConfig) {
            throw new Error(`Unknown endpoint name: ${endointName}`);
        }

        return foundConfig;
    }

    getStoreKey(enpointName, params) {
        return this._getUrlByName(enpointName, params);
    }
}

export const feedApi = new FeedApi();
