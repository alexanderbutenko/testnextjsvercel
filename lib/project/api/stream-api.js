import qs from 'qs';
import { baseApi } from './base-api';

const API_URL = 'https://pro14anywhere.streamamg.com/api/v1/';
const SEARCH_PATH = '/66bad7cc-6af1-4c10-b833-d6fb6b0f5452/qWfGTXFxDSyNcdqFA4NKBipZaun0ovODGS9kfzMLHx29Ip4RL8/en/search/';

class StreamApi {
    getKSessionToken = (entryId, accessToken) => {
        const params = {
            entryId,
            apijwttoken: accessToken
        };

        return baseApi
            .get('https://pro14payments.streamamg.com/api/v1/session/ksession?' + qs.stringify(params));
    };


    getSearchUrl = (metaData) => {
        if (!metaData) {
            return `${API_URL}${SEARCH_PATH}`;
        }

        return `${API_URL}${SEARCH_PATH}?query=NOT(metaData.SysEntryEntitlements:(paid)) AND ${generateMetaDataQS(metaData)}`;
    };

    getSearchUrlByVideoId = (id) => {
        if (!id) return `${API_URL}${SEARCH_PATH}`;

        return `${API_URL}${SEARCH_PATH}?query=mediaData.entryId:${id}`
    }
}

export const streamApi = new StreamApi();

export const generateMetaDataQS = (metaData) => {
    const metaDataQS = [];

    const generateSQItem = (name, value) => {
        return `metaData.${name}:(${value})`;
    };

    Object.keys(metaData).forEach((group) => {
        let QSItem;

        if (Array.isArray(metaData[group])) {
            QSItem = metaData[group].map((value) => generateSQItem(group, value)).join(' AND ');
        } else {
            QSItem = generateSQItem(group, metaData[group]);
        }

        metaDataQS.push(QSItem);
    });

    return metaDataQS.join(' AND ');
};
