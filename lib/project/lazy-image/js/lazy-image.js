import React from 'react';

const LazyImage = (
    {
        image,
        childCss,
        onLoad,
        ...rest
    }
) => {
    if (!image) return null;

    return <img
        className={`${childCss} lazyload`}
        key={image}
        alt=""
        data-srcset={image}
        data-sizes="auto"
        onLoad={onLoad}
        {...rest}
    />;
};

LazyImage.defaultProps = {
    childCss: '',
    onLoad: () => {}
};

export default LazyImage;
