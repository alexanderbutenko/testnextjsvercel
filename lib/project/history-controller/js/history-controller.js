import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';

class HistoryController {
    constructor() {
        // this.prevState = window.history.state;
        this.prevState = {}
        this.subscribers = {};

        // this.init();
    }

    init() {
        window.addEventListener('popstate', this._onPopstate);
    }

    destroy() {
        window.removeEventListener('popstate', this._onPopstate);
    }

    _onPopstate = (event) => {
        const prevId = get(this.prevState, 'id', null);
        const id = get(event, 'state.id', null);
        const prevInnerState = get(this.prevState, 'state', null);
        const newInnerState = get(event.state, 'state', null);

        // inform component to clean its state
        // return (prevInnerState, newInnerState, shouldClean)
        if (prevId !== id && this.subscribers[prevId]) {
            this.subscribers[prevId].cb(prevInnerState, newInnerState, true);
        }

        if (id && this.subscribers[id]) {
            this.subscribers[id].cb(prevInnerState, newInnerState, false);
        }

        this.prevState = event.state;
    };

    subscribeOnPopstate = (id, cb) => {
        if (this.subscribers[id]) {
            console.log(`Found registered component with id: ${id}. Data will be overridden.`);
        }

        this.subscribers[id] = { cb };

        return () => {
            delete this.subscribers[id];
        };
    };

    getState = () => {
        return get(window.history.state, 'state', null);
    };

    _generateState(id, title, url, state = {}) {
        if (!id) {
            throw new Error('You must provide components id');
        }

        const historyState = {
            id,
            state: cloneDeep(state)
        };

        if (!historyState.state.url) {
            historyState.state.url = url;
        }

        return historyState;
    }

    pushState = (id, title, url, state) => {
        const historyState = this._generateState(id, title, url, state);

        window.history.pushState(historyState, title, url);
        this.prevState = window.history.state;
    };

    replaceState = (id, title, url, state = {}) => {
        const historyState = this._generateState(id, title, url, state);

        window.history.replaceState(historyState, title, url);
    }
}

const instance = new HistoryController();
export default instance;
