import isObject from 'lodash/isObject';

const PROFILE_LS_KEY = 'cookie-profile';
const DEFAULT_PROFILE = {
    functional: true,
    performance: true,
    hasInteracted: false
};

class CookieProfile {
    constructor() {
        this.state = {
            profile: this._getDataFromLocalStorage(),
        };
    }

    getProfile = () => {
        return this.state.profile;
    };

    updatePreferences(formData) {
        this.state.profile = {
            ...this.state.profile,
            performance: formData.performance === 'true',
            hasInteracted: true,
        };

        this._setDataToLocalStorage();
    }

    _getDataFromLocalStorage() {
        let data;

        try {
            data = JSON.parse(window.localStorage.getItem(PROFILE_LS_KEY));
        } catch (error) {
            console.log(error.message);
        }

        return isObject(data) ? data : DEFAULT_PROFILE;
    }

    _setDataToLocalStorage() {
        window.localStorage.setItem(
            PROFILE_LS_KEY,
            JSON.stringify(this.state.profile)
        );
    }
}

export default new CookieProfile();
