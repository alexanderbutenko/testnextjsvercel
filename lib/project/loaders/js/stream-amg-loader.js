import { app } from '@/lib/project/general';

class StreamAmgLoader {
    constructor() {
        this.state = {
            loaders: {},
        };
    }

    load() {
        const url = app.state.streamAmgSettings?.url;

        if (!url) {
            throw new Error('You should provide streamAMG script URL in global state');
        }

        if (this.state.loaders[url]) return this.state.loaders[url];

        this.state.loaders[url] = new Promise((resolve, reject) => {
            const script = document.createElement('script');

            script.src = url;
            script.async = true;
            script.onload = () => {
                resolve(script);
            };
            script.onerror = (error) => {
                reject(error);
            };
            document.body.appendChild(script);
        });

        return this.state.loaders[url];
    }
}

const instance = new StreamAmgLoader();

export default instance;
