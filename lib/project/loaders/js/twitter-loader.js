import { Observable } from '@/lib/foundation/observable';

class TwitterLoader {
    constructor() {
        this.onLoadObservable = new Observable();
    }

    loadScript() {
        window.twttr = (function (d, s, id) {
            const fjs = d.getElementsByTagName(s)[0];
            const t = window.twttr || {};

            if (d.getElementById(id)) return t;

            const js = d.createElement(s);
            js.id = id;
            js.src = 'https://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);

            t._e = [];
            t.ready = function (f) {
                t._e.push(f);
            };

            return t;
        }(document, 'script', 'twitter-wjs'));

        window.twttr.ready(this.onLoadObservable.notify);
    }

    subscribeOnLoad = (cb) => {
        if (window.twttr && window.twttr.init && typeof cb === 'function') {
            cb();
        }

        this.loadScript();
        return this.onLoadObservable.subscribe(cb);
    }
}

const instance = new TwitterLoader();
export default instance;
