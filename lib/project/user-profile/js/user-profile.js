import { nanoid } from 'nanoid';
import qs from 'qs';
import isObject from 'lodash/isObject';
import { isValid, format, parseISO } from 'date-fns';
import Cookies from 'js-cookie';
import { Observable } from '@/lib/foundation/observable';
import { getTeamConfigPropById } from '@/lib/project/helpers';
import { fanScoreService } from '@/lib/project/services';
import { app } from '@/lib/project/general';

const PROFILE_LS_KEY = 'user-profile';
const CLIENT_ID = app.getClientId();
const DEFAULT_PROFILE = {
    auth: null,
    details: null,
    preferences: null,
};

class UserProfile {
    constructor() {
        this.onChangeObservable = new Observable();
        this.onLoginObservable = new Observable();
        this.state = {
            profile: this._getDataFromLocalStorage(),
            isLoggedIn: false,
        };

        if (!this.state.profile.deviceId) {
            this.state.profile.deviceId = nanoid();
            // this._setDataToLocalStorage();
        }

        this._checkLoggedIn();
    }

    isLoggedIn() {
        return this.state.isLoggedIn;
    }

    getProfile = () => {
        return this.state.profile;
    };

    login = (formData) => {
        if (!formData) return Promise.reject(new Error('Missed input `formData`'));

        const { deviceId } = this.state.profile;
        const params = {
            grant_type: 'password',
            client_id: CLIENT_ID,
            deviceId,
            username: formData.email,
            password: formData.password,
        };
        return fanScoreService
            .login(qs.stringify(params))
            .then((response) => {
                this._login(response.data);
                this._getDetails();
                this._getPreferences();
                this._getFavouriteTeam();
            });
    };

    logout = () => {
        const { auth } = this.state.profile;

        if (!auth || !auth.access_token) return;

        fanScoreService
            .logout(auth.access_token)
            .then(() => {
                this._logout();
            })
            .catch(() => {
                console.log('Can not logout from FanScore');
            });
    };

    forgottenPassword = (email) => {
        if (!email) return Promise.reject(new Error('Missed input `email`'));

        return fanScoreService
            .forgottenPasword(CLIENT_ID, { email });
    };

    changePassword = (formData) => {
        if (!formData) return Promise.reject(new Error('Missed input `formData`'));

        const { auth } = this.state.profile;
        const params = {
            oldPassword: formData.oldPassword,
            newPassword: formData.password,
        };

        return fanScoreService
            .changePassword(auth.access_token, params);
    };

    register = (formData) => {
        if (!formData) return Promise.reject(new Error('Missed input `formData`'));

        const { deviceId } = this.state.profile;
        const registerData = {
            grant_type: 'register',
            client_id: CLIENT_ID,
            username: formData.email,
            password: formData.password,
            deviceId
        };

        return fanScoreService
            .register(qs.stringify(registerData))
            .then((response) => {
                this._login(response.data);
            });
    };

    updateDetails = (formData) => {
        if (!formData) return Promise.reject(new Error('Missed input `formData`'));

        const { auth } = this.state.profile;
        const params = {
            firstName: formData.firstName,
            lastName: formData.lastName,
            birthDate: new Date(formData.birthDate).toISOString(),
            country: formData.country,
            postcode: formData.postcode
        };

        return fanScoreService
            .updateProfile(auth.access_token, params)
            .then((response) => {
                this.state.profile.details = this._mapDetails(response.data.data);
                this._notifyOnChange();
            });
    };

    updatePreferences = (formData) => {
        if (!formData) return Promise.reject(new Error('Missed input `formData`'));

        const { auth } = this.state.profile;
        const preferencesData = {
            optedToNews: formData.optedToNews,
            optedToOffers: formData.optedToOffers,
            optedToSharePersonalDetails: formData.optedToSharePersonalDetails
        };

        const params = Object.keys(preferencesData).map((propName) => {
            const marketingKey = this._getMarketingKeyByPreferenceName(propName);

            return ({
                key: marketingKey,
                email: preferencesData[propName] === 'true'
            });
        });

        return fanScoreService
            .updatePreferences(CLIENT_ID, auth.access_token, params)
            .then((response) => {
                this.state.profile.preferences = this._mapPreferences(response.data.data);
                this._notifyOnChange();
            });
    };

    updateFavouriteTeam = (formData) => {
        if (!formData) return Promise.reject(new Error('Missed input `formData`'));

        const { auth } = this.state.profile;
        const fancscoreId = getTeamConfigPropById(Number(formData.teamId), 'fanscoreId');
        const params = {
            selectedOptionIds: [fancscoreId]
        };

        return fanScoreService
            .updateFavouriteTeam(auth.access_token, params)
            .then(() => {
                this.state.profile.team = {
                    fanscoreId: fancscoreId,
                    optaId: Number(formData.teamId)
                };
                this._notifyOnChange();
            });
    };

    _login(auth) {
        this.state.profile.auth = auth;
        this.state.profile.auth.lastUpdated = new Date();
        this.state.isLoggedIn = true;
        Cookies.set('auth', `Bearer ${auth.access_token}`, { expires: auth.expires_in / 24 / 60 / 60 });
        // this._setDataToLocalStorage();
        this._notifyOnChange();
        this._notifyOnLogin();
    }

    _logout() {
        delete this.state.profile.auth;
        this.state.isLoggedIn = false;
        Cookies.remove('auth');
        // this._setDataToLocalStorage();
        this._notifyOnChange();
    }

    _checkLoggedIn() {
        const { auth, deviceId } = this.state.profile;

        if (!auth || !auth.lastUpdated) return;

        const today = new Date();
        const lastUpdated = new Date(auth.lastUpdated);

        if (Math.ceil((today - lastUpdated) / 1000) > auth.expires_in) {
            const params = {
                grant_type: 'refresh_token',
                client_id: CLIENT_ID,
                refresh_token: auth.refresh_token,
                deviceId
            };

            fanScoreService
                .refreshToken(qs.stringify(params))
                .then((response) => {
                    this._login(response.data);
                    this._getDetails();
                    this._getPreferences();
                    this._getFavouriteTeam();
                })
                .catch(() => {
                    this._logout();
                });
        } else {
            this.state.isLoggedIn = true;
            this._getDetails();
            this._getPreferences();
            this._getFavouriteTeam();
        }
    }

    _getDetails() {
        const { auth } = this.state.profile;

        fanScoreService
            .getProfile(auth.access_token)
            .then((response) => {
                this.state.profile.details = this._mapDetails(response.data.data);
                this._notifyOnChange();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    _mapDetails(details) {
        return ({
            ...details,
            birthDate: isValid(parseISO(details.birthDate)) ?
                format(parseISO(details.birthDate), 'yyyy-MM-dd') :
                details.birthDate,
        });
    }

    _getPreferenceNameByMarketingKey(key) {
        switch (key) {
            case 'CLIENT':
                return 'optedToNews';
            case 'SPONSOR':
                return 'optedToOffers';
            case 'SUPPORTED_CLUB':
                return 'optedToSharePersonalDetails';
            default:
                return '';
        }
    }

    _getMarketingKeyByPreferenceName(name) {
        switch (name) {
            case 'optedToNews':
                return 'CLIENT';
            case 'optedToOffers':
                return 'SPONSOR';
            case 'optedToSharePersonalDetails':
                return 'SUPPORTED_CLUB';
            default:
                return '';
        }
    }

    _mapPreferences(marketingPreferences) {
        if (!Array.isArray(marketingPreferences)) {
            return {};
        }

        const preferences = {};

        marketingPreferences.forEach((marketingPreference) => {
            const propName = this._getPreferenceNameByMarketingKey(marketingPreference.key);

            preferences[propName] = marketingPreference.preferences.email;
        });

        return preferences;
    }

    _getPreferences() {
        const { auth } = this.state.profile;

        fanScoreService
            .getPreferences(CLIENT_ID, auth.access_token)
            .then((response) => {
                this.state.profile.preferences = this._mapPreferences(response.data.data);
                this._notifyOnChange();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    _mapFavouriteTeam(data) {
        const foundOption = data.options.find((option) => {
            return option.selected;
        });

        if (!foundOption) {
            return ({
                fanscoreId: null,
                optaId: null,
            });
        }

        return ({
            fanscoreId: foundOption.id,
            optaId: Number(foundOption.metadata?.optaId)
        });
    }

    _getFavouriteTeam() {
        const { auth } = this.state.profile;

        fanScoreService
            .getFavouriteTeam(CLIENT_ID, auth.access_token)
            .then((response) => {
                this.state.profile.team = this._mapFavouriteTeam(response.data.data);
                this._notifyOnChange();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    subscribeOnChange = (cb) => {
        return this.onChangeObservable.subscribe(cb);
    };

    _notifyOnChange() {
        this.onChangeObservable.notify(this.state.profile);
    }

    subscribeOnLogin = (cb) => {
        return this.onLoginObservable.subscribe(cb);
    };

    _notifyOnLogin() {
        this.onLoginObservable.notify(this.state.profile);
    }

    _getDataFromLocalStorage() {
        let data;

        try {
            data = JSON.parse(window.localStorage.getItem(PROFILE_LS_KEY));
        } catch (error) {
            console.log(error.message);
        }

        return isObject(data) ? data : DEFAULT_PROFILE;
    }

    _setDataToLocalStorage() {
        window.localStorage.setItem(
            PROFILE_LS_KEY,
            JSON.stringify({ auth: this.state.profile.auth })
        );
    }
}

export default new UserProfile();
