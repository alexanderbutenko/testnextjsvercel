import { app } from '@/lib/project/general';
import { FetchService } from '@/lib/project/services';

const CLIENT_ID = app.getClientId();

class PromoService {
    constructor() {
        this.state = {
            campaigns: {}
        };
    }

    fetchCampaign(promoId) {
        if (this.state.campaigns[promoId]) {
            return this.state.campaigns[promoId];
        }

        this.state.campaigns[promoId] = new Promise((resolve) => {
            const fetchService = new FetchService({
                config: {
                    url: `https://promoblocks-cms-api.incrowdsports.com/v1/${CLIENT_ID}/campaigns/content/${promoId}`
                },
                successCallback: (data) => resolve(data),
            });

            fetchService.once();
        });

        return this.state.campaigns[promoId];
    }
}

const instance = new PromoService();
export default instance;
