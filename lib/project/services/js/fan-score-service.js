import { baseApi } from '@/lib/project/api';

const OAUTH_ORIGIN = 'https://oauth.fanscore.com';
const PROFILE_ORIGIN = 'https://profile.fanscore.com';

class FanScoreService {
    login(data) {
        return baseApi.post(`${OAUTH_ORIGIN}/oauth/token`, data);
    }

    logout(accessToken) {
        return baseApi.delete(`${OAUTH_ORIGIN}/oauth/token/revoke`,
            this._getAuthHeaders(accessToken));
    }

    register(data) {
        return baseApi.post(`${OAUTH_ORIGIN}/oauth/token`, data);
    }

    forgottenPasword(clientId, data) {
        return baseApi.post(`${OAUTH_ORIGIN}/users/forgotPassword?clientId=${clientId}`, data);
    }

    changePassword(accessToken, data) {
        return baseApi.post(`${OAUTH_ORIGIN}/users/me/changePassword`,
            data,
            this._getAuthHeaders(accessToken));
    }

    refreshToken(data) {
        return baseApi.post(`${OAUTH_ORIGIN}/oauth/token`, data);
    }

    getProfile(accessToken) {
        return baseApi.get(`${PROFILE_ORIGIN}/v1/profile/me`, this._getAuthHeaders(accessToken));
    }

    updateProfile(accessToken, data) {
        return baseApi.patch(`${PROFILE_ORIGIN}/v1/profile/me`,
            data,
            this._getAuthHeaders(accessToken));
    }

    getPreferences(clientId, accessToken) {
        return baseApi.get(`${PROFILE_ORIGIN}/v1/contactpreferences?clientId=${clientId}`,
            this._getAuthHeaders(accessToken));
    }

    updatePreferences(clientId, accessToken, data) {
        return baseApi.post(`${PROFILE_ORIGIN}/v2/contactpreferences?clientId=${clientId}`,
            data,
            this._getAuthHeaders(accessToken));
    }

    getFavouriteTeam(clientId, accessToken) {
        return baseApi.get(`${PROFILE_ORIGIN}/v1/clientpreferences/favouriteTeam?clientId=${clientId}`,
            this._getAuthHeaders(accessToken));
    }

    updateFavouriteTeam(accessToken, data) {
        return baseApi.post(`${PROFILE_ORIGIN}/v1/clientpreferences`,
            data,
            this._getAuthHeaders(accessToken));
    }

    _getAuthHeaders(accessToken) {
        return ({
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    }
}

const instance = new FanScoreService();
export default instance;
