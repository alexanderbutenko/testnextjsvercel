import axios from 'axios';

const DEFAULT_OPTIONS = {
    config: {
        timeout: 60000,
    },
    updateInSeconds: null,
    keepUpdated: false,
    beforeFetch: () => {},
    afterFetch: () => {},
    successCallback: () => {},
    errorCallback: () => {},
};

class FetchService {
    constructor(options = {}) {
        this._checkConfig(options.config);
        this.setOptions(options);
        this.timeoutId = null;
        this.timeoutTimestamp = null;
        this.source = null;
    }

    destroy() {
        this.cancel();
        this.state = null;
    }

    _checkConfig(config) {
        if (!config || !config.url) {
            throw new Error('You must provide url property within config');
        }
    }

    setOptions(options) {
        this._checkConfig(options.config);

        this.state = {
            ...DEFAULT_OPTIONS,
            ...options
        };
    }

    once = () => {
        this._fetch();
    };

    many = () => {
        this.state.keepUpdated = true;
        this._fetch();
    };

    cancel = () => {
        clearTimeout(this.timeoutId);
        this.timeoutId = null;
        this.timeoutTimestamp = null;
        if (this.source) {
            this.source.cancel();
            this.source = null;
        }

        this.state.keepUpdated = false;
    };

    _fetch() {
        const { config } = this.state;

        this.timeoutTimestamp = new Date();
        const CancelToken = axios.CancelToken;
        this.source = CancelToken.source();

        if (this.state.beforeFetch) {
            this.state.beforeFetch();
        }

        const newConfig = { ...config, cancelToken: this.source.token };

        axios(newConfig)
            .catch((thrown) => {
                if (axios.isCancel(thrown)) {
                    return;
                }

                if (this.state.errorCallback) {
                    this.state.errorCallback(thrown);
                }

                if (this.state.afterFetch) {
                    this.state.afterFetch();
                }

                if (this.state.keepUpdated) {
                    this._fixedFetch();
                }
            })
            .then((response) => {
                if (!response) return;

                try {
                    if (this.state.successCallback) {
                        this.state.successCallback(response.data);
                    }
                } catch (error) {
                    // TODO: call global error handler
                    console.log(error);
                }

                if (this.state.afterFetch) {
                    this.state.afterFetch();
                }

                if (this.state.keepUpdated) {
                    this._fixedFetch();
                }
            });
    }

    _fixedFetch() {
        const { updateInSeconds } = this.state;

        if (!updateInSeconds) return;

        const difference = new Date() - this.timeoutTimestamp;
        const waitTime = (updateInSeconds * 1000 - difference);

        this.timeoutId = setTimeout(this._fetch.bind(this), waitTime);
        this.timeoutTimestamp = new Date();
    }
}

export default FetchService;
