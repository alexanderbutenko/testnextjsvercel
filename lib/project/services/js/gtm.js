import { cookieProfile } from '@/lib/project/cookie-profile';

class GtmService {
    _send(event, data) {
        const profile = cookieProfile.getProfile();

        // Do not track if user not agree
        if (!profile.performance) return;

        if (window.dataLayer) {
            window.dataLayer.push({ event, ...data });
        }
    }

    push(category, action, label) {
        this._send('eventTrack', {
            eventCategory: category,
            eventAction: action,
            eventLabel: label
        });
    }
}

export default new GtmService();
