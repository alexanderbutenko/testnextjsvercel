import { endpointNames, feedApi } from '@/lib/project/api';
import FetchService from './fetch-service';

class BannerService {
    constructor() {
        this.state = {
            sections: null,
            hasData: false,
        };
        this.fetchDataPromise = null;
    }

    fetchData() {
        if (this.fetchDataPromise) return this.fetchDataPromise;

        this.fetchDataPromise = new Promise((resolve) => {
            const url = feedApi.getStoreKey(endpointNames.BANNERS);
            const fetchService = new FetchService({
                config: {
                    url
                },
                successCallback: (data) => {
                    resolve(data);
                },
            });

            fetchService.once();
        });

        return this.fetchDataPromise;
    }

    getPlacement = (sectionId, groupId, placementId) => {
        if (!sectionId || !groupId || !placementId) {
            return Promise.reject(new Error(`Invalid input data: ${sectionId}, ${groupId}, ${placementId}`));
        }

        return new Promise((resolve) => {
            this.fetchData().then((data) => {
                if (!Array.isArray(data.sections)) return resolve(null);

                const foundSection = data.sections.find((section) =>
                    section.id === sectionId);

                if (!foundSection || !Array.isArray(foundSection.groups)) return resolve(null);

                const foundGroup = foundSection.groups.find((group) =>
                    group.id === groupId);

                if (!foundGroup || !Array.isArray(foundGroup.placements)) return resolve(null);

                const foundPlacement = foundGroup.placements.find((placement) =>
                    placement.id === placementId);

                if (foundPlacement) return resolve(foundPlacement);

                return resolve(null);
            });
        });
    }
}

const instance = new BannerService();
export default instance;
