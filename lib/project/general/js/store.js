import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import appReducers from '@/lib/ducks';

const store = createStore(
    appReducers,
    {},
    applyMiddleware(thunk)
);

export default store;

export const withStore = (children) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    );
};
