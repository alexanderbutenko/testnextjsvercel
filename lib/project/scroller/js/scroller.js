import anime from 'animejs';

class Scroller {
    constructor(element, options = {}) {
        this.element = element;

        this.defaultOptions = {
            scrollOffset: 0,
            duration: 1000,
        };

        this.options = { ...this.defaultOptions, ...options };
    }

    destroy = () => {
        anime.remove(document.documentElement);
        anime.remove(document.body);
    };

    getElementDocumentPosition() {
        const box = this.element.getBoundingClientRect();

        return {
            top: box.top + window.pageYOffset,
            bottom: box.bottom + window.pageYOffset,
        };
    }

    scrollTo(coordinate) {
        const topCoordinate = coordinate - this.options.scrollOffset;

        anime({
            targets: document.documentElement,
            scrollTop: topCoordinate,
            duration: this.options.duration,
            easing: 'easeInOutSine'
        });

        anime({
            targets: document.body,
            scrollTop: topCoordinate,
            duration: this.options.duration,
            easing: 'easeInOutSine'
        });
    }

    scrollToTop(safe = false) {
        const topCoordinate = this.getElementDocumentPosition(this.element).top;

        if (safe && this.isInViewport(topCoordinate)) {
            return;
        }

        this.scrollTo(topCoordinate);
    }

    isInViewport(coordinate) {
        return this.getViewportTop() <= coordinate && this.getViewportBottom() >= coordinate;
    }

    getViewportTop() {
        return window.pageYOffset;
    }

    getViewportHeight() {
        return document.documentElement.clientHeight;
    }

    getViewportBottom() {
        return this.getViewportTop() + this.getViewportHeight();
    }

    scrollToBottom() {
        const bottomCoordinate = this.getElementDocumentPosition(this.element).bottom;

        this.scrollTo(bottomCoordinate);
    }
}

export default Scroller;
