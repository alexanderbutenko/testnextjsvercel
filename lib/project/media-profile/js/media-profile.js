import isObject from 'lodash/isObject';
import { isValid, parseISO } from 'date-fns';

const PROFILE_LS_KEY = 'media-profile';
const DEFAULT_PROFILE = {
    profile: {},
    isLoggedIn: false,
};
// 30 minutes
const SESSION_TIME_IN_SEC = 30 * 60;

class MediaProfile {
    constructor() {
        this.state = {
            profile: this._getDataFromLocalStorage(),
            isLoggedIn: false,
        };

        this._checkLoggedIn();
    }

    isLoggedIn = () => {
        return this.state.isLoggedIn;
    };

    login = () => {
        this.state.profile.lastUpdated = new Date();
        this.state.isLoggedIn = true;
        this._setDataToLocalStorage();
    };

    logout = () => {
        this.state = DEFAULT_PROFILE;
        this._setDataToLocalStorage();
    };

    _checkLoggedIn() {
        const { profile } = this.state;

        if (!profile.lastUpdated || !isValid(parseISO(profile.lastUpdated))) {
            this.logout();
            return;
        }

        const today = new Date();
        const lastUpdated = new Date(profile.lastUpdated);
        if (Math.ceil((today - lastUpdated) / 1000) < SESSION_TIME_IN_SEC) {
            this.state.isLoggedIn = true;
        } else {
            this.logout();
        }
    }

    _getDataFromLocalStorage() {
        let data;

        try {
            data = JSON.parse(window.localStorage.getItem(PROFILE_LS_KEY));
        } catch (error) {
            console.log(error.message);
        }

        return isObject(data) ? data : {};
    }

    _setDataToLocalStorage() {
        window.localStorage.setItem(
            PROFILE_LS_KEY,
            JSON.stringify(this.state.profile)
        );
    }
}

export default new MediaProfile();
