export const renderTypes = Object.freeze({
    WINDOW: 'window',
    POPUP: 'popup'
});
