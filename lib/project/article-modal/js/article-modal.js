import get from 'lodash/get';
import { nanoid } from 'nanoid';
import { eventBus } from '@/lib/project/event-bus';
import { userProfile } from '@/lib/project/user-profile';
import { baseApi, isHtmlContentType } from '@/lib/project/api';
import { historyController } from '@/lib/project/history-controller';
// import { eventTypes as modalEventTypes, Modal } from '@/lib/shared/modal';
import { eventTypes as modalEventTypes } from '@/lib/shared/modal';
// import { pageSpinner } from '@/lib/shared/page-spinner';
import { renderTypes } from './enums';

const HISTORY_ID = 'ArticleModal';

class ArticleModal {
    constructor() {
        this.state = {
            sessionId: nanoid(),
            chainStartUrl: null,
            isOpenning: false,
            modal: null,
        };
        this.modalCache = {};
        this.unsubscribeHistoryController = historyController.subscribeOnPopstate(
            HISTORY_ID,
            this._onHistoryChange
        );

        this.unsubscribeOnLogin = userProfile.subscribeOnLogin(this._resetCache);
    }

    open = (url) => {
        if (!url) return;

        if (!this.state.chainStartUrl) {
            this.state.chainStartUrl = window.location.href;
        }

        this._openModal(url);
        this._setHistoryState(url);
    };

    _openModal(url) {
        if (this.state.isOpenning) return;

        this.state.isOpenning = true;

        this._getModal(url).then((modal) => {
            try {
                modal.open();
                this.state.isOpenning = false;
                this.state.modal = modal;

                setTimeout(() => {
                    let a = document.querySelectorAll(".richtext a");
                    if (a.length > 0) {
                        a.forEach(link => link.setAttribute("target", "_blank"));
                    }
                }, 1500);

            } catch (error) {
                console.log(error);
            }
        }).catch((error) => {
            this.state.isOpenning = false;
            console.log(error);
        });
    }

    _closeModal() {
        if (this.state.modal) {
            this.state.modal.close();
        }
    }

    _getModal(url) {
        if (this.modalCache[url]) {
            return Promise.resolve(this.modalCache[url]);
        }

        return this._getTarget(url)
            .then((data) => {
                try {
                    this.modalCache[url] = Modal.create(data, {
                        onOpen: () => {
                            setTimeout(() => eventBus.emit(modalEventTypes.OPEN), 100);
                        },
                        onClose: () => {
                            this._onClose(url);
                        },
                    });

                    return this.modalCache[url];
                } catch (error) {
                    return Promise.reject(error);
                }
            }).catch((error) => {
                // pageSpinner.hide();
                return Promise.reject(error);
            });
    }

    _getAsyncUrl(url) {
        return url + `${this._getDelimeter(url)}modal=true`;
    }

    _getDelimeter(url) {
        if (url.includes('?')) return '&';

        return '?';
    }

    _getTarget(url) {
        let asyncUrl = this._getAsyncUrl(url);
        const headers = this._getAuthHeaders();

        if (headers) {
            asyncUrl += '&login=true';
        }

        // pageSpinner.show();

        return baseApi.get(asyncUrl, headers)
            .then((response) => {
                if (isHtmlContentType(response)) {
                    return Promise.resolve(response.data);
                }

                //return Promise.reject('Unknown result from async data');
                return Promise.resolve(response.data.html);
            }).catch((error) => {
                return Promise.reject(error);
            })
            .finally(() => {
                // pageSpinner.hide();
            });
    }

    _getAuthHeaders() {
        const profile = userProfile.getProfile();
        const accessToken = get(profile, 'auth.access_token') || null;

        if (!accessToken) return null;

        return ({
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    }

    _setHistoryState(url) {
        historyController.pushState(
            HISTORY_ID,
            null,
            url,
            {
                renderType: renderTypes.POPUP,
                sessionId: this.state.sessionId
            }
        );
    }

    _onHistoryChange = (prevState, newState, shouldClean) => {
        if (shouldClean) {
            this._cleanContent(prevState);
        } else {
            this._updateContent(prevState, newState);
        }
    };

    _updateContent(prevState, newState) {
        const { sessionId } = this.state;

        if (prevState && prevState.sessionId === sessionId &&
            newState.sessionId !== sessionId
        ) {
            this._closeModal();
            return;
        }

        if (newState.sessionId === sessionId) {
            if (newState.renderType === renderTypes.POPUP) {
                this._openModal(newState.url);
            } else if (newState.renderType === renderTypes.WINDOW) {
                this._closeModal();
            }
        } else {
            // after page reload flow, we always reload pages
            // instead of to show modal
            window.location.reload();
        }
    }

    _cleanContent(prevState) {
        if (prevState.sessionId === this.state.sessionId) {
            this._closeModal();
        } else {
            window.location.reload();
        }
    }

    _onClose = (url) => {
        const innerState = historyController.getState();

        eventBus.emit(modalEventTypes.CLOSE);

        // finishing popup chain
        if (innerState && innerState.url === url) {
            historyController.pushState(
                HISTORY_ID,
                null,
                this.state.chainStartUrl,
                {
                    renderType: renderTypes.WINDOW,
                    sessionId: this.state.sessionId
                }
            );
            this.state.chainStartUrl = '';
        }
    };

    _resetCache = () => {
        Object.keys(this.modalCache).forEach((url) => {
            this.modalCache[url].destroy();
            delete this.modalCache[url];
        });
    }
}

const instance = new ArticleModal();
export default instance;
