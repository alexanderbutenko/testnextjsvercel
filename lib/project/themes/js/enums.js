import { app } from 'project/general';

export const getThemeByTeamId = (id) => {
    const teams = app.state.teams || [];
    const foundTeam = teams.find((team) => team.id === id);

    if (!foundTeam) return '';

    return `theme-${foundTeam.name.toLowerCase().split(' ').join('-')}`;
};
