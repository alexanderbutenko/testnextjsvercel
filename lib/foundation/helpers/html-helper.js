// if (!Element.prototype.matches) {
//     Element.prototype.matches = Element.prototype.msMatchesSelector;
// }

export default class HtmlHelper {
    static createElement(htmlString) {
        if (htmlString.indexOf('<html') !== -1) {
            throw new Error('Trying to create element from the complete html page. Partial html is required');
        }

        const template = document.createElement('template');
        template.innerHTML = htmlString.trim();
        const element = template.content ? template.content.firstChild : template.firstChild;
        // move element to current document to avoid bugs later when we insert it to DOM
        document.adoptNode(element);
        return element;
    }

    static hasElementParentWithSelector(target, selector, includeSelf = true, root) {
        return this.getParent(target, selector, includeSelf, root) !== null;
    }

    static getParent(target, selector, includeSelf = true, root = document.body) {
        if (includeSelf && target.matches(selector)) {
            return target;
        }

        let parent = target.parentElement;

        while (parent && parent !== root) {
            if (parent.matches(selector)) {
                return parent;
            }
            parent = parent.parentElement;
        }

        return null;
    }

    static isHtmlElement(element) {
        return typeof element === 'object' && ('nodeType' in element);
    }
}
