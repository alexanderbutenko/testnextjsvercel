import { combineReducers } from 'redux';
import feeds from './feeds.reducer';
//import cloudMatrix from './cloud-matrix.reducer';

const appReducers = combineReducers({
    feeds,
    //cloudMatrix
});

export default appReducers;
