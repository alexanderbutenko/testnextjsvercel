import get from 'lodash/get';

const FEED_RECEIVE = 'FEED_RECEIVE';

const reducer = (state = {}, action) => {
    /* eslint-disable no-case-declarations */
    switch (action.type) {
        case FEED_RECEIVE:
            if (!action.payload) return state;

            const storedLastUpdated = get(state, `${action.url}.lastUpdated`, '');
            const updateRequired = storedLastUpdated < action.payload.lastUpdated;

            if (!state[action.url] || updateRequired) {
                return {
                    ...state,
                    [action.url]: action.payload
                };
            }
            return state;
        default:
            return state;
    }
};

export default reducer;

export const receiveData = (name, data) => ({
    type: FEED_RECEIVE,
    url: name,
    payload: data
});
