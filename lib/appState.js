import appSettings from '@/lib/appsettings.json';

export const appState = Object.freeze({
    mediaConfig: appSettings.AppSettings.MediaConfig,
    streamAmgSettings: appSettings.StreamAmgSettings,
});
