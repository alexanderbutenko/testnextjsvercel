import React from 'react';
import Link from 'next/link';

const IndexPage = (): JSX.Element => {
    return (
        <Link href={'/latest/'}>
            <a>Latest</a>
        </Link>
    );
};

export default IndexPage;
