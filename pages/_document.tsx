import React from 'react';
import Document, {
    Html,
    Head,
    Main,
    NextScript
} from 'next/document';

export default class MyDocument extends Document {
    render(): JSX.Element {
        return (
            <Html className='html'>
                <Head />
                <body className="body">
                    <div className="page">
                        <Main />
                        <NextScript />
                    </div>
                </body>
            </Html>
        );
    }
}
