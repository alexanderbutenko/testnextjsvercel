import React from 'react';
import Head from 'next/head';
import { GetStaticProps } from 'next';
import { AxiosResponse } from 'axios';
import { Endpoints } from '@/lib/endpoints';
import { NewsFeed } from '@/components/news-feed';
import { axiosNode } from '@/lib/project/services/js/axios-node';
import { ILatestNews, IArticleLatestNews } from '@interfaces';

interface IGetStaticProps {
    props: {
        articles: IArticleLatestNews[] | null;
    };

    revalidate?: number;
}

const NewsFeedPage = (props: ILatestNews): JSX.Element => {
    return (
        <>
            <Head>
                <title>Next.js - News Feed</title>
            </Head>
            <NewsFeed {...props} />
        </>
    );
};

export const getStaticProps: GetStaticProps = async (): Promise<IGetStaticProps> => {
    const response: AxiosResponse<IArticleLatestNews[]> = await axiosNode.get(Endpoints.LatestNews + '?page=1&limit=5');
    const articles = response.data;

    return {
        props: {
            articles,
        },

        revalidate: 1,
    };
};

export default NewsFeedPage;
