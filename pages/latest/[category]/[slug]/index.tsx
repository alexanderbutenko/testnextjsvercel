import qs from 'qs';
import React from 'react';
import Head from 'next/head';
import Error from 'next/error';
import { useRouter } from 'next/router';
import { GetStaticProps, GetStaticPaths } from 'next';
import { AxiosResponse, AxiosError } from 'axios';
import Custom404 from '@/pages/404';
import { Article } from '@/components/article';
import { Endpoints } from '@/lib/endpoints';
import { axiosNode } from '@/lib/project/services';
import {
    IArticle,
    IArticlePage,
    IAllArticles,
    IArticleBySlug, IArticleLatestNews
} from '@interfaces';

interface IGetStaticProps {
    props: {
        article: IArticlePage | null;
        statusCode: boolean | number;
    };

    revalidate?: number;
}

const ArticlePage = ({ article, statusCode }: IArticlePage): JSX.Element => {
    const router = useRouter();

    if (router.isFallback) {
        return <div>Loading...</div>;
    }

    if (statusCode) {
        return <Error statusCode={statusCode} />;
    }

    if (!article) {
        return (
            <>
                <Head>
                    <meta name="robots" content="noindex" />
                </Head>
                <Custom404 />
            </>
        );
    }

    return (
        <>
            <Head>
                <title>
                    {`Next.js - ${article.title}`}
                </title>
            </Head>
            <section>
                <Article article={article} />
            </section>
        </>
    );
};

export const getStaticPaths = async () => {
    const response: AxiosResponse<IArticleLatestNews[]> = await axiosNode.get(Endpoints.LatestNews);
    const articles = response.data;
    return {
        paths: articles.map((elem) => {
            return {
                params: {
                    category: elem.category,
                    slug: elem.id,
                }
            };
        }),

        fallback: true,
    };
};

// @ts-ignore
export const getStaticProps = async ({ params }) => {
    let article: IArticleBySlug | null = null;
    let statusCode: boolean | number = false;

    await axiosNode
        .get(`${Endpoints.LatestNews}/${params.slug}`)
        .then(({ data }: AxiosResponse<IArticleBySlug>): void => {
            article = data;
        })
        .catch(({ response }: AxiosError): void => {
            if (response) {
                statusCode = response.status;
            }
        });

    return {
        props: {
            article,
            statusCode
        },

        revalidate: 1,
    };
};

export default ArticlePage;
