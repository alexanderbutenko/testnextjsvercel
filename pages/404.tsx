import React from 'react';

export default class Custom404 extends React.Component {
    render(): JSX.Element {
        return <h1>404 - Page Not Found</h1>;
    }
}
