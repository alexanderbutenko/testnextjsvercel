import React from 'react';
import { NewsFeedListing } from '@/components/news-feed-listing';
import { getPattern } from './pattern';

const LatestNewsLayout = ({ items }) => {
    if (!Array.isArray(items)) return null;

    const renderGroupsListing = () => {
        const patternSize = 5;
        const groupsList = [];
        const totalGroups = Math.ceil(items.length / patternSize);

        for (let i = 0; i < totalGroups; i++) {
            const startIndex = i * patternSize;
            const endIndex = (i + 1) * patternSize;
            const groupItems = items.slice(startIndex, endIndex);

            groupsList.push(
                <NewsFeedListing
                    key={i}
                    pattern={getPattern()}
                    items={groupItems}
                />
            );
        }

        return groupsList;
    };

    return renderGroupsListing();
};

export default LatestNewsLayout;
