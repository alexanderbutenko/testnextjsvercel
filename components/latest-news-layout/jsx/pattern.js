import { rowNames, rowPattern } from '@/lib/project/general';

export const getPattern = () => [
    rowPattern[rowNames.TALL],
    rowPattern[rowNames.TRIPLE_ONE_THIRD],
];
