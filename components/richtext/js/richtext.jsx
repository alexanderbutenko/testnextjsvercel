import styles from '../scss/richtext.module.scss';

import React from 'react';
import MarkdownIt from 'markdown-it';

const Richtext = ({ model }) => {
    const md = new MarkdownIt();
    let content = model.content;

    if (!model.isHtml) {
        content = md.render(content);
    }

    return (
        <div className={styles.richtext} dangerouslySetInnerHTML={{ __html: content }} />
    );
};

export default Richtext;
