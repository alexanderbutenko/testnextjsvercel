import styles from '../scss/image.module.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { generateBemCssModule } from '@/lib/project/helpers';
import { DamImage } from '@/lib/shared/dam-image';

const Image = ({ model, bemList }) => {
    const rootCss = generateBemCssModule('image', bemList, styles);

    return (
        <a href={model.link} target="_blank" rel="noopener noreferrer">
            <figure className={rootCss}>
                <div className={`${styles['image__bg']} of-cover`}>
                    <DamImage image={model.image} />

                </div>
                {
                    model.title &&
                    <figcaption className={styles['image__title']}>{model.title}</figcaption>
                }
            </figure>
        </a>
    );
};

Image.defaultProps = {
    bemList: PropTypes.Array
};

export default Image;
