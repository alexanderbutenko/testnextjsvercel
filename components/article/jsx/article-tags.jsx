import cn from 'classnames/bind';
import React from 'react';
import Link from 'next/link';
import styles from '../scss/article-tags.module.scss';

const ArticleTags = ({ childCss, categories }) => {

    const cx = cn.bind(styles);

    return (
        <div className={cx(childCss, 'article-tags')}>
            <div className="container-tiny">
                <ul className={cx('article-tags__list')}>
                    <li className={cx('article-tags__item')}
                    >
                        <Link
                            href={'/latest/index'}
                            as={`/latest/${categories}`}
                        >
                            <a className={cx('article-tags__link')}>
                                {categories}
                            </a>
                        </Link>
                    </li>

                </ul>
            </div>
        </div>
    );
};

ArticleTags.defaultProps = {
    childCss: '',
};

export default ArticleTags;
