import React from 'react';
import styles from '../scss/article-preview.module.scss';

const ArticlePreview = ({ preview }) => {
    return (
        <div className={styles['article-preview']}>
            <div className={styles['article-preview__content']}>
                <div className="richtext">
                    <p>{preview} Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
        </div>
    );
};

export default ArticlePreview;
