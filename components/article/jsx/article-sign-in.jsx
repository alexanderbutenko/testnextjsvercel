import React from 'react';
// import { app } from 'project/general';
// import { gtmService } from 'project/services';
// import { DamImage } from 'shared/dam-image';
// import { formTypes, ssoController } from 'shared/sso';

const ArticleSignIn = ({ title }) => {
    const onClick = () => {
        ssoController.show(formTypes.SIGN_IN);

        // gtmService.push(app.state.siteSection, 'Sign In / Register', `Premium Articles > ${title}`);
    };

    return (
        <div className="article-sign-in">
            <div className="container-tiny">
                <div className="article-sign-in__bg fill-parent of-cover">
                    {/*<DamImage image={'7b677161-4e6b-4db0-8398-ffaab5a13085.jpg'} />*/}
                </div>
                <div className="article-sign-in__title">
                    <span className="article-sign-in__white">To continue reading</span> sign in
                    <span className="article-sign-in__white">&nbsp;or</span> register for free
                </div>
                <button
                    className="article-sign-in__btn primary-button"
                    type="button"
                    onClick={onClick}
                >
                    Sign in / register
                </button>
            </div>
        </div>
    );
};

export default ArticleSignIn;
