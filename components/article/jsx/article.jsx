import cn from 'classnames/bind';
import React from 'react';
import dynamic from 'next/dynamic';
// import { LatestNewsCarousel } from 'components/latest-news-carousel';
import { ArrayHelper } from '@/lib/foundation/helpers';
import { ArticleContent } from '@/lib/shared/article-content';
import { NewsCard } from '@/components/news-card';
import ArticleHero from './article-hero';
import ArticleAuthor from './article-author';
// import ArticleSignIn from './article-sign-in';
import ArticlePreview from './article-preview';
import styles from '../scss/article.module.scss';

const cx = cn.bind(styles);
const DynamicArticleTags = dynamic(() => import('./article-tags'));

class Article extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isShowArticleTags: false,
        };
    }

    isUserLoggedIn() {
        // const { article } = this.props;

        // return (article.auth && article.auth.loginRequired) && !userProfile.isLoggedIn();
        return true;
    }

    showArticleTags = () => {
        this.setState((prevState) => ({
            isShowArticleTags: !prevState.isShowArticleTags
        }));
    }

    render() {
        const { article, latestNewsCarousel } = this.props;
        if (!article) return null;

        return (
            <div className={cx('article')}>
                <ArticleHero {...this.props} />
                <div className={cx('article__body')}>
                    <ArticleContent {...this.props} />
                    {
                        this.isUserLoggedIn() &&
                        <div className={cx('article__preview', 'container-tiny')}>
                            <ArticlePreview preview={article.content} />
                        </div>
                    }
                </div>
                {
                    !article.singlePage &&
                    <div className={cx('article__show-tags-wrapper', 'container-tiny')}>
                        <button
                            className={cx('article__show-tags-button', 'primary-button')}
                            onClick={this.showArticleTags}
                        >
                            {this.state.isShowArticleTags ? 'Hide tags' : 'Show tags'}
                        </button>
                    </div>
                }
                {
                    this.state.isShowArticleTags &&
                    <DynamicArticleTags childCss={cx('article__tags')} categories={article.category} />
                }
                <ArticleAuthor
                    childCss={cx('article__author')}
                    author={article.author}
                    title={article.title}
                />
                {
                    article.latestNewsCarousel &&
                    !ArrayHelper.isEmpty(article.latestNewsCarousel) &&
                    <div className={cx('article__carousel')}>
                        <div className={cx('article__carousel-container', 'container')}>
                            <div className={cx('article-carousel')}>
                                <h2>Related News</h2>
                                {/*<LatestNewsCarousel*/}
                                {/*    title={article.latestNewsCarousel.title}*/}
                                {/*    articles={article.latestNewsCarousel.articles}*/}
                                {/*    allLatestUrl="/latest"*/}
                                {/*/>*/}
                                {
                                    article.latestNewsCarousel.map((item) =>
                                        (
                                            <NewsCard
                                                article={item}
                                                bemList={['tall']}
                                                childCss={cx('article-carousel__item')}
                                                key={item.id}
                                            />
                                        ))
                                }
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default Article;
