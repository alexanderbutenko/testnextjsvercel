import React from 'react';
import { generateBemCss } from '@/lib/project/helpers';
// import { SpriteSvg } from 'project/sprite-svg';
import { DamImage } from '@/lib/shared/dam-image';
import styles from '../scss/article-author.module.scss';

const ALLOWED_SOCIAL_PROVIDERS = ['TWITTER'];

const ArticleAuthor = ({ childCss, author, title }) => {
    if (!author || !author.name) return null;

    return (
        <div className={`${childCss} ${styles['article-author']}`}>
            <div className="container-mid">
                <div className="article-author__content">
                    {
                        author.imageUrl &&
                        <div className="article-author__avatar of-cover">
                            <DamImage image={author.imageUrl} />
                        </div>
                    }
                    <div className="article-author__info">
                        <p className="article-author__title">Author</p>
                        <div className="article-author__social">
                            <p className="article-author__name">{author.name}</p>
                            {
                                author.socialHandles.map((socialHandle) => {
                                    if (!ALLOWED_SOCIAL_PROVIDERS.includes(socialHandle.provider)) {
                                        return null;
                                    }

                                    const iconCss = generateBemCss(
                                        'article-author__account-icon',
                                        [socialHandle.provider.toLowerCase()]
                                    );

                                    return (
                                        <a
                                            key={socialHandle.link}
                                            className="article-author__link"
                                            href={socialHandle.link}
                                            rel="noreferrer noopener"
                                            target="_blank"
                                            onClick={() => {}}
                                        >
                                            <span className={iconCss}>
                                                {/*<SpriteSvg*/}
                                                {/*    iconName={socialHandle.provider.toLowerCase()}*/}
                                                {/*    width={15}*/}
                                                {/*    height={15}*/}
                                                {/*/>*/}
                                            </span>
                                            <span className="article-author__account">{socialHandle.handle}</span>
                                        </a>
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

ArticleAuthor.defaultProps = {
    childCss: '',
};

export default ArticleAuthor;
