import React from 'react';
import { format, parseISO } from 'date-fns';
import { contentTypes } from '@/lib/project/general';
import { DamImage } from '@/lib/shared/dam-image';
import { Image } from '@/components/image';
import { Video } from '@/components/video';
import styles from '../scss/article-hero.module.scss';

const ArticleHero = ({ article }) => {
    const {
        id,
        title,
        content,
        category,
        publishDate,
        author,
        avatar,
        image,
        readTime
    } = article;

    const getArticleInfo = () => {
        return {
            title,
            url: document.location.href,
        };
    };

    const renderContent = (content) => {
        switch (content.contentType) {
            case contentTypes.IMAGE:
                return <Image model={content} bemList={['flexible', 'no-title']} />;
            case contentTypes.VIDEO:
                return <Video
                    model={content}
                    gtm={getArticleInfo()}
                    bemList={['big', 'flexible']}
                />;
            default:
                console.log('Unknown type:', content.contentType);
                return null;
        }
    };

    return (
        <div className={styles['article-hero']}>
            {
                <div className='container'>
                    <div className={styles['article-hero__wrap']}>
                        {
                            category &&
                            <div className={styles['article-hero__label']}>{category}</div>
                        }
                        <h1 className={styles['article-hero__title']}>{title}</h1>
                        <div className={styles['article-hero__bottom']}>
                            {
                                author &&
                                <>
                                    {
                                        avatar &&
                                        <div className={`${styles['article-hero__avatar']} of-cover`}>
                                            <DamImage image={avatar} />
                                        </div>
                                    }
                                    <span className={styles['article-hero__author']}>{author}</span>
                                </>
                            }
                            <span className={styles['article-hero__reading-time']}>{readTime} min read</span>
                            {
                                publishDate &&
                                <time className={styles['article-hero__date']} dateTime={publishDate}>
                                    {format(parseISO(publishDate), 'd MMM yyyy')}
                                </time>
                            }
                        </div>
                    </div>
                </div>
            }
            <div className={`${styles['article-hero__media']} of-cover`}>
                <DamImage image={image} />
            </div>
        </div>
    );
};

export default ArticleHero;
