import cn from 'classnames';
import React from 'react';
import Link from 'next/link';
import { format, parseISO } from 'date-fns';
import { generateModuleName, generateThumbnailSrc, getCategoryString } from '@/lib/project/helpers';
import { contentTypes } from '@/lib/project/general';
import { SpriteSvg } from '@/lib/project/sprite-svg';
import { DamImage } from '@/lib/shared/dam-image';
import styles from '../scss/index.module.scss';

export default class NewsCard extends React.Component {
    render() {
        const { article, childCss, bemList } = this.props;
        const {
            id,
            title,
            content,
            category,
            publishDate,
            author,
            avatar,
            image
        } = article;

        const typeMod = content?.toLowerCase();

        const bemlist = bemList || [];

        const bemCss = generateModuleName('newsCard', styles, [...bemlist, typeMod]);
//
        return (
            <Link
                href={'/latest/[category]/[slug]'}
                as={`/latest/${category}/${id}`}
            >
                <a className={cn(childCss, styles.newsCard, bemCss)}>
                    <div className={cn(styles.image, 'of-cover')}>
                        <DamImage image={image} />
                        <SpriteSvg childCss={styles.iconPlay} iconName="play" width={18} height={16} />
                        <div className={cn(styles.memberLogo, 'of-contain')}>
                            <DamImage image="c5fcf2d0-d489-4fa2-b8b4-1f267e871e29.png" />
                        </div>
                    </div>
                    <div className={styles.content}>
                        <div className={styles.topWrap}>
                            {
                                category &&
                                <p className={styles.label}>{category}</p>
                            }
                            {
                                publishDate &&
                                <time className={styles.date} dateTime={publishDate}>
                                    {format(parseISO(publishDate), 'd MMM yyyy')}
                                </time>
                            }
                        </div>
                        <h3 className={styles.title}>{title}</h3>
                        <div className={styles.bottom}>
                            {
                                author &&
                                <div className={styles.author}>
                                    {
                                        avatar &&
                                        <div className={cn(styles.avatar, 'of-cover')}>
                                            <DamImage image={avatar} />
                                        </div>
                                    }
                                    <span className={styles.authorName}>{author}</span>
                                </div>
                            }
                            {
                                article.readTimeMinutes > 0 &&
                                <div className={styles.readingTime}>
                                    {article.readTimeMinutes} min read
                                </div>
                            }
                            {
                                publishDate &&
                                <time className={cn(styles.date, styles.dateBottom)}>
                                    dateTime={publishDate}
                                    {format(parseISO(publishDate), 'd MMM yyyy')}
                                </time>
                            }
                            <div
                                className={cn(styles.memberLogo, 'of-contain')}>
                                <DamImage image="c5fcf2d0-d489-4fa2-b8b4-1f267e871e29.png" />
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        );
    }
}
