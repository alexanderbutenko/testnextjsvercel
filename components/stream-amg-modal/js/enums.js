export const VIDEO_ID_QS_NAME = 'popupVideoId';

export const getVideoExternalLink = (url, id) => {
    return `${url}?${VIDEO_ID_QS_NAME}=${id}`;
};
