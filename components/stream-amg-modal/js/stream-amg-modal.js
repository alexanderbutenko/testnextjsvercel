import React from 'react';
import reactDOM from 'react-dom';
import { historyController } from '@/lib/project/history-controller';
// import { ReactModal } from '@/lib/shared/modal';
import { StreamAmgPlayer, playerTypes } from '@/components/stream-amg-player';
//import { VIDEO_ID_QS_NAME }from './enums';

const HISTORY_ID = 'StreamAmgModal';

class StreamAmgModal {
    constructor() {
        // this.root = document.createElement('div');
        // this.root.id = 'stream-amg-modal';
        // document.body.appendChild(this.root);

        this.state = {
            entryId: null,
            options: {},
            gtm: null,
            prevHistoryUrl: null,
        };
        this.modal = null;

        this.init();
    }

    componentDidMount() {
        this.root = document.createElement('div');
        this.root.id = 'stream-amg-modal';
        document.body.appendChild(this.root);
    }

    init() {
        //const videoId = this.getVideoIdFromQS();

        //if (videoId) {
        //    this.show(videoId);
        //}
    }

    destroy() {
        reactDOM.unmountComponentAtNode(this.root);
    }

    //getVideoIdFromQS() {
    //    return UrlHelper.getParamFromLocation(VIDEO_ID_QS_NAME, window.location);
    //}

    show = (video, gtm, options = {}) => {
        if (!video || typeof video.entryId !== 'string') return;

        this.state.entryId = video.entryId;
        this.state.options = options;
        this.state.gtm = gtm;
        this.render();

        const seoTitle = this.getSeoTitle(video.title);

        this.state.prevHistoryUrl = window.location.href;
        historyController.replaceState(HISTORY_ID, null, `/video/${seoTitle}/${video.entryId}`);
    };

    hide = () => {
        this.state.entryId = null;
        this.state.options = {};
        this.state.gtm = null;
        this.render();

        historyController.replaceState(HISTORY_ID, null, this.state.prevHistoryUrl);
        this.state.prevHistoryUrl = null;
    };

    getSeoTitle(title) {
        if (typeof title !== 'string') return '';

        return title.replace(/\s+/g, '-').toLocaleLowerCase();
    }

    render() {
        const { entryId, options, gtm } = this.state;
        const model = {
            ...options,
            entryId,
            playerType: playerTypes.EMBED
        };
        const modalOptions = {
            bemList: ['light', 'close-outside']
        };

        // reactDOM.render(
        //     <ReactModal
        //         options={modalOptions}
        //         isOpen={entryId !== null}
        //         onClose={this.hide}
        //     >
        //         {
        //             entryId !== null &&
        //             <StreamAmgPlayer model={model} gtm={gtm} />
        //         }
        //     </ReactModal>,
        //     this.root
        // );

        reactDOM.render(<StreamAmgPlayer model={model} gtm={gtm} />, this.root);
    }
}

export default new StreamAmgModal();
