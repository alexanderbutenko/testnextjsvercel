import React from 'react';
import Glide from '@glidejs/glide';
import { app } from '@/lib/project/general';
// import { gtmService } from 'project/services';
import { ArrowDisabler } from '@/lib/project/glide';
import { Image } from '@/components/image';
import { CarouselButtons } from '@/lib/shared/carousel-buttons';

class Gallery extends React.Component {
    constructor(props) {
        super(props);
        this.glideOptions = {
            perView: 1,
            rewind: false,
            bound: true,
            gap: 32,
        };
        this.carousel = null;
        this.rootRef = React.createRef();
        this.cardRefs = new Map();
    }

    componentDidMount() {
        //fix rendering in modal
        setTimeout(() => {
            this.initCarousel();
        }, 0);
    }

    componentDidUpdate() {
        this.destroyCarousel();
        this.initCarousel();
    }

    componentWillUnmount() {
        this.destroyCarousel();
    }

    initCarousel() {
        this.carousel = new Glide(this.rootRef.current, {
            ...this.glideOptions,
        });

        this.carousel.on('run.before', () => {
            this.cardRefs.forEach((el) => {
                el.classList.add('is-active');
            });
        });

        this.carousel.on('run.after', () => {
            this.setActiveSlides(this.carousel.index, this.carousel.settings.perView);
        });

        this.carousel.on('mount.after', () => {
            this.setActiveSlides(this.carousel.index, this.carousel.settings.perView);
        });

        this.carousel.mount({ ArrowDisabler });
    }

    destroyCarousel() {
        if (this.carousel) {
            this.glideOptions.startAt = this.carousel.index;
            this.carousel.destroy();
        }
    }

    setActiveSlides(currentIndex, perView) {
        this.cardRefs.forEach((el, i) => {
            if (i >= currentIndex && i < currentIndex + perView) {
                el.classList.add('is-active');
            } else {
                el.classList.remove('is-active');
            }
        });
    }

    onClickCarouselBtn = (action) => {
        // gtmService.push(app.state.siteSection, action, `Image Gallery > > ${document.location.href}`);
    };

    render() {
        const { children, title } = this.props.model;

        return (
            <div className="gallery">
                <div className="glide glide--base" ref={this.rootRef}>
                    {
                        title &&
                        <div className="gallery__title">{title}</div>
                    }
                    <div className="gallery__wrap">
                        <div className="glide__track" data-glide-el="track">
                            <ul className="glide__slides">
                                {
                                    children.map((content, i) => {
                                        return (
                                            <li
                                                className="glide__slide"
                                                key={i}
                                                ref={(el) => {
                                                    this.cardRefs.set(i, el);
                                                }}
                                            >
                                                <Image model={content} bemList={['gallery']} />
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                        </div>
                        <div className="gallery__controls">
                            <CarouselButtons
                                onClickPrevBtn={this.onClickCarouselBtn}
                                onClickNextBtn={this.onClickCarouselBtn}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Gallery;
