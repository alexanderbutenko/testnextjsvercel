import React from 'react';
import ReactDOM from 'react-dom';
// import { BaseComponent } from 'project/general';
import LatestNewsCarousel from './latest-news-carousel';

class LatestNewsCarouselComponent extends BaseComponent {
    static getNamespace() {
        return 'latest-news-carousel';
    }

    static getRequiredRefs() {
        return ['data'];
    }

    onInit() {
        ReactDOM.render(
            <LatestNewsCarousel {...this.options} {...this.initData} />,
            this.element
        );
    }

    onDestroy() {
        ReactDOM.unmountComponentAtNode(this.element);
    }
}

export default LatestNewsCarouselComponent;
