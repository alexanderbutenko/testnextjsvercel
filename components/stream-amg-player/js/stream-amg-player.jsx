import cn from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import merge from 'lodash/merge';
import get from 'lodash/get';
import set from 'lodash/set';
import { customAlphabet } from 'nanoid';
import { generateModuleName } from '@/lib/project/helpers';
import { app } from '@/lib/project/general';
import { streamAmgLoader } from '@/lib/project/loaders';
import { eventBus } from '@/lib/project/event-bus';
import { eventTypes as modalEventTypes } from '@/lib/shared/modal';
import { playerTypes, eventTypes } from './enums';
import styles from '../scss/stream-amg-player.module.scss';

class StreamAmgPlayer extends React.Component {
    constructor(props) {
        super(props);

        this.rootRef = React.createRef();
        // target id is used for id attribute so it can't start from the number
        // also it can be start from ~ so we have to pass custom alphabet
        this.targetId = customAlphabet('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', 10)();
        this.playerNode = null;
        this.autoPlay = get(this.getOptions(), 'autoPlay', false);
        this.type = get(this.getOptions(), 'type', playerTypes.EMBED);
    }

    getOptions() {
        const { model, gtm } = this.props;

        return {
            type: model.playerType,
            autoPlay: model.autoPlay,
            widgetParams: {
                ...model.widgetParams,
                targetId: this.targetId,
                entry_id: model.entryId,
            },
            gtm
        };
    }

    componentDidMount() {
        if (this.type === playerTypes.EMBED) {
            this.embed();
            //return;
        }
    }

    componentWillUnmount() {
        if (window.kWidget) {
            window.kWidget.destroy(this.targetId);
        }

        eventBus.removeListener(modalEventTypes.CLOSE, this.onModalClose);
        eventBus.removeListener(eventTypes.PLAY, this.onCustomEventPlay);
    }

    embed() {
        streamAmgLoader
            .load()
            .then(() => {
                this.handleEmbed();
            })
            .catch(() => {
            });
    }

    handleEmbed() {
        const { widgetParams, type } = this.getOptions();

        const params = merge(
            {},
            app.state.streamAmgSettings?.widgetParams,
            widgetParams,
            {
                readyCallback: (playerId) => {
                    this.playerNode = document.getElementById(playerId);
                    this.onPlayerReady();
                }
            },
        );

        this.applyAutoPlay(params, type);
        window.kWidget.embed(params);
    }

    applyAutoPlay(params) {
        // just ignore autoPlay if it is passed as true because basically current logic doesn't interpreter
        // correctly what it means to be an autoPlay video
        // the only case we REALLY want to auto play video is stream pages
        // other videos should be played only after USER GESTURE which means that they are not "autoPlay" at all
        // those will be triggered to play programmatically
        // after kWidget.embed method is called and onMediaReady event is received
        if (this.autoPlay) {
            set(params, 'flashvars.autoPlay', true);
        } else {
            set(params, 'flashvars.autoPlay', false);
            set(params, 'captureClickEventForiOS', true);
        }
    }

    onPlayerReady = () => {
        eventBus.addListener(modalEventTypes.CLOSE, this.onModalClose);
        eventBus.addListener(eventTypes.PLAY, this.onCustomEventPlay);

        this.playerNode.kBind('doPlay', () => {
            eventBus.emit(eventTypes.PLAY, { playerNode: this.playerNode });
            // this.sendToGtm('Play');
        });

        this.playerNode.kBind('doPause', () => {
            // this.sendToGtm('Pause');
        });

        // autoPlay videos are supposed to play without user gesture and without sound
        // if this is not the case, we starts video programmatically
        if (!this.autoPlay) {
            this.playerNode.kBind('mediaReady', () => {
                this.playerNode.sendNotification('doPlay');
            });
        }
    };

    onModalClose = () => {
        this.stopPlaying();
    };

    onCustomEventPlay = ({ playerNode }) => {
        if (playerNode === this.playerNode) return;

        this.stopPlaying();
    };

    stopPlaying() {
        this.playerNode.sendNotification('doPause');
    }

    render() {
        const rootCss = generateModuleName('streamAmgPlayer', styles, this.props.bemList);

        return (
            <div className={cn(styles.streamAmgPlayer)} id={this.targetId} />
        );
    }
}

StreamAmgPlayer.propTypes = {
    model: PropTypes.shape({
        entryId: PropTypes.string,
        widgetParams: PropTypes.object,
        playerType: PropTypes.string,
        autoPlay: PropTypes.bool
    })
};

export default StreamAmgPlayer;
