import React from 'react';
import { generateBemCss } from '@/lib/project/helpers';
import { DamImage } from '@/lib/shared/dam-image';

const Section = ({ children, bemList }) => {
    const rootCss = generateBemCss('section', bemList);

    const getBackgroundImageSource = () => {
        if (!Array.isArray(bemList)) return null;

        if (bemList.includes('dark')) {
            return '2bd5b7f6-eef7-4702-9c34-988650bde499.png?quality=100';
        }

        if (bemList.includes('light')) {
            return '7d6a4e38-3fda-41a5-8fcb-5e667e1de480.png?quality=100';
        }

        return null;
    };

    const bgSrc = getBackgroundImageSource();

    return (
        <section className={rootCss}>
            {
                bgSrc &&
                <div className="section__bg fill-parent of-cover">
                    <DamImage image={bgSrc} />
                </div>
            }
            <div className="container">
                {children}
            </div>
        </section>
    );
};

export default Section;
