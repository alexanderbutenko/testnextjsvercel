import React from 'react';
import { generateBemCss } from '@/lib/project/helpers';

const Quote = ({ model, bemList }) => {
    const rootCss = generateBemCss('quote', bemList);
    return (
        <div className={rootCss}>
            <blockquote className="quote__text">“{model.text}</blockquote>
            <div className="quote__bottom">
                <div className="quote__author">{model.author}</div>
                <div className="quote__title">{model.title}</div>
            </div>
        </div>
    );
};

export default Quote;
