import React, { useState } from 'react';
import { FetchService } from '@/lib/project/services';
import { BlockSpinner } from '@/lib/shared/block-spinner';
import { Container } from '@/components/container';
import { LatestNewsLayout } from '@/components/latest-news-layout';
import styles from '../scss/news-feed.module.scss';
import { Endpoints } from '../../../lib/endpoints';

const NewsFeed = (props) => {
    const [articles, setArticles] = useState(props.articles);
    const [isFetching, setIsFetching] = useState(false);
    const [count, setCount] = useState(2);
    const [limit, setLimit] = useState(5);

    const loadMore = () => {
        const latestNewsFetchService = new FetchService({
            config: {
                url: `${Endpoints.LatestNews}?page=${count}&limit=${limit}`,
            },
            beforeFetch: () => setIsFetching(true),
            successCallback: (data) => onSuccessFetch(data),
        });

        latestNewsFetchService.once();
        setCount(count + 1);
    };

    const onSuccessFetch = (data) => {
        setArticles([...articles, ...data]);
        setIsFetching(false);
    };

    return (
        <>
            <section className={styles.newsFeed}>
                <Container>
                    <LatestNewsLayout items={articles} />
                    <div className={styles.footer}>
                        <button
                            className="text-button text-button--icon text-button--border"
                            onClick={() => loadMore()}
                        >
                            Load More
                        </button>
                    </div>
                </Container>
            </section>
            {
                isFetching &&
                <BlockSpinner hasOverlay />
            }
        </>
    );
};

export default NewsFeed;
