import React from 'react';
import ArrayHelper from '@/lib/foundation/helpers/array-helper';
import { getBemList, mapCloudVideo } from '@/lib/project/helpers';
import { FetchService } from '@/lib/project/services';
import { streamApi } from '@/lib/project/api/stream-api';
import { StreamAmgCard } from '@/components/stream-amg-card';
import { playbackTypes } from '@/components/stream-amg-player';

class Video extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cloudVideo: null,
        };

        this.fetchService = null;
    }

    componentDidMount() {
        const { model } = this.props;

        if (model && model.sourceSystemId) {
            this.fetchService = new FetchService({
                config: {
                    url: streamApi.getSearchUrlByVideoId(model.sourceSystemId)
                },
                successCallback: (data) => {
                    if (!data || ArrayHelper.isEmpty(data.itemData)) return;

                    this.setState({
                        cloudVideo: mapCloudVideo(data.itemData[0])
                    });
                }
            });

            this.fetchService.once();
        }
    }

    componentWillUnmount() {
        if (this.fetchService) {
            this.fetchService.destroy();
            this.fetchService = null;
        }
    }

    render() {
        const { model, gtm, bemList } = this.props;
        const { cloudVideo } = this.state;
        // incrowd have 4 types of videos,
        // so if we need addition we will add switch here

        if (!cloudVideo) return null;

        let bemlist = getBemList(bemList);

        if (!model.title) {
            bemlist = getBemList(bemList, ['no-title']);
        }

        return <StreamAmgCard
            video={cloudVideo}
            gtm={gtm}
            playbackType={playbackTypes.INLINE}
            bemList={bemlist}
        />;
    }
}

export default Video;
