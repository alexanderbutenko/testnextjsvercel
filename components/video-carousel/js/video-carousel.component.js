import React from 'react';
import ReactDOM from 'react-dom';
import { BaseComponent } from '@/lib/project/general';
import VideoCarousel from './video-carousel';

class VideoCarouselComponent extends BaseComponent {
    static getNamespace() {
        return 'video-carousel';
    }

    static getRequiredRefs() {
        return ['data'];
    }

    onInit() {
        ReactDOM.render(
            <VideoCarousel
                options={this.options}
                bemList={this.options.bemList}
                {...this.initData} />,
            this.element
        );
    }

    onDestroy() {
        ReactDOM.unmountComponentAtNode(this.element);
    }
}

export default VideoCarouselComponent;
