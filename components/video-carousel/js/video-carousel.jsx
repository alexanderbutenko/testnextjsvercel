import React from 'react';
import { FetchService } from '@/lib/project/services';
import {
    mapCloudVideo,
    mapCloudVideos,
    sortVideosByReleaseFrom,
    excludePaidVideos,
} from '@/lib/project/helpers';
import { BlockSpinner } from '@/lib/shared/block-spinner';
import { Section } from '@/components/section';
import { Carousel } from '@/components/carousel';
import { StreamAmgCard } from '@/components/stream-amg-card';
import { playbackTypes } from '@/components/stream-amg-player';

class VideoCarousel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            isFetching: false
        };
        this.fetchService = null;
    }

    componentDidMount() {
        this.fetchService = new FetchService({
            config: {
                url: this.props.endpointUrl
            },
            beforeFetch: () => { this.setState({ isFetching: false }); },
            successCallback: (data) => {
                this.setState({
                    videos: mapCloudVideos(data)
                        .filter(excludePaidVideos)
                        .sort(sortVideosByReleaseFrom),
                    isFetching: false
                });
            },
            errorCallback: () => {
                this.setState({ isFetching: false });
            }
        });

        this.fetchService.once();
    }

    componentWillUnmount() {
        if (this.fetchService) {
            this.fetchService.destroy();
        }
    }

    render() {
        if (!this.props.endpointUrl) return null;

        const {
            title,
            isTitleSmall,
            targetUrl,
            bemList,
            sponsor
        } = this.props;
        const { videos, isFetching } = this.state;

        if (isFetching) {
            return (
                <Section bemList={bemList}>
                    <BlockSpinner bemList={['light']} />
                </Section>
            );
        }

        if (!videos.length) return null;

        const topVideos = videos.slice(0, 10).map((video) =>
            <StreamAmgCard
                key={video.id}
                video={mapCloudVideo(video)}
                playbackType={playbackTypes.POPUP}
            />);

        return (
            <Section bemList={bemList}>
                <Carousel
                    title={title}
                    slides={topVideos}
                    sponsor={sponsor}
                    targetUrl={videos.length > 10 ? targetUrl : null}
                    targetUrlText={'All videos'}
                    bemList={bemList}
                    isTitleSmall={isTitleSmall}
                    onIndexChanged={this.getButtonsState}
                />
            </Section>
        );
    }
}

VideoCarousel.defaultProps = {
    options: {}
};

export default VideoCarousel;
