import cn from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import { isValid, format, parseISO } from 'date-fns';
import { generateThumbnailSrc, generateModuleName } from '@/lib/project/helpers';
import { SpriteSvg } from '@/lib/project/sprite-svg';
import { DamImage } from '@/lib/shared/dam-image';
import styles from '../scss/stream-amg-preview.module.scss';

const StreamAmgPreview = ({ model, bemList }) => {
    const rootCss = generateModuleName('streamAmgPreview', styles, bemList);
    const thumbnailSrc = generateThumbnailSrc(model.entryId);

    const getDuration = () => {
        if (!model.duration) return null;

        const hours = Math.floor(model.duration / 60 / 60) || null;
        const minutes = Math.floor(model.duration / 60 - hours * 60);
        const seconds = model.duration - (hours * 60 * 60 + minutes * 60);

        const duration = [];

        if (hours > 0) {
            duration.push(hours);
            duration.push(minutes < 10 ? '0' + minutes : minutes);
        } else {
            duration.push(minutes);
        }

        duration.push(seconds < 10 ? '0' + seconds : seconds);

        return duration.filter((value) => value !== null).join(':');
    };

    const getLastUpdated = () => {
        if (!isValid(parseISO(model.releasedFrom))) return null;

        return format(parseISO(model.releasedFrom), 'dd MMM yyyy');
    };

    const duration = getDuration();
    const releasedFrom = getLastUpdated();

    return (
        <div className={`${rootCss}`}>
            <div className={styles.top}>
                <div className={cn(styles.bg, 'fill-parent', 'of-cover')}>
                    {
                        model.videoThumbnail ?
                            <DamImage image={model.videoThumbnail} /> :
                            <DamImage image={thumbnailSrc} />
                    }
                </div>
                <div className={styles.play}>
                    <SpriteSvg iconName="play" width={20} height={20} />
                </div>
            </div>
            <div className={styles.content}>
                {
                    model.label &&
                    <p className={styles.label}>{model.label}</p>
                }
                <h3 className={styles.title}>{model.title}</h3>
                {
                    (duration || releasedFrom) &&
                    <div className={styles.bottom}>
                        {
                            duration &&
                            <div className={styles.duration}>
                                <SpriteSvg iconName="play" width={7} height={7} />
                                <div>{duration}</div>
                            </div>
                        }
                        {
                            releasedFrom &&
                            <span className={styles.date}>{releasedFrom}</span>
                        }
                    </div>
                }
            </div>
        </div>
    );
};

StreamAmgPreview.propTypes = {
    model: PropTypes.shape({
        entryId: PropTypes.string,
        title: PropTypes.string,
        categories: PropTypes.array,
        duration: PropTypes.number,
        lastUpdated: PropTypes.string,
    }),
};

export default StreamAmgPreview;
