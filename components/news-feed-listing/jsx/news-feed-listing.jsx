import styles from '../scss/index.module.scss';
import React from 'react';
import { generateBemCss, generateBemCssModule } from '@/lib/project/helpers';
import { NewsCard } from '@/components/news-card';

export default class NewsFeedListing extends React.Component {
    getRowPattern(itemIndex) {
        const { pattern } = this.props;

        if (!Array.isArray(pattern)) return null;

        const totalSlots = pattern.reduce((acc, cur) => acc + cur.slots.length, 0);
        const slotIndex = itemIndex % totalSlots;
        let total = 0;

        for (let i = 0; i < pattern.length; i++) {
            const row = pattern[i];

            total += row.slots.length;

            if (slotIndex <= total - 1) {
                return row;
            }
        }

        return null;
    }

    renderRow(pattern, cardsList, index) {
        const rowCss = generateBemCssModule('news-feed-row', pattern.modifiers, styles);
        const gridCss = generateBemCssModule('news-feed-grid', pattern.modifiers, styles);

        return (
            <div className={rowCss} key={index}>
                <div className={gridCss}>
                    {
                        cardsList.map((card, i) => {
                            const slot = pattern.slots[i];
                            const slotCss = generateBemCssModule('news-feed-slot', slot.modifiers, styles);

                            return (
                                <div className={slotCss} key={card.id}>
                                    <NewsCard article={card} bemList={slot.modifiers} />
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }

    renderCards() {
        const { items } = this.props;
        const grid = [];
        let i = 0;
        let index = 0;

        while (i < items.length) {
            const rowPattern = this.getRowPattern(i);

            if (!rowPattern) return null;

            grid.push(this.renderRow(
                rowPattern,
                items.slice(i, i + rowPattern.slots.length),
                index,
            ));

            i += rowPattern.slots.length;
            index++;
        }

        return grid;
    }

    render() {
        if (!Array.isArray(this.props.items)) return null;

        return this.renderCards();
    }
}
