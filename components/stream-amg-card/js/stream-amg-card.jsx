import React from 'react';
import PropTypes from 'prop-types';
import { ArrayHelper } from '@/lib/foundation/helpers';
import { userProfile } from '@/lib/project/user-profile';
import { streamApi } from '@/lib/project/api';
import { getBemList } from '@/lib/project/helpers';
import { BlockSpinner } from '@/lib/shared/block-spinner';
// import { streamAmgModal } from '@/components/stream-amg-modal';
import {
    playbackTypes,
    playerTypes,
    StreamAmgPlayer
} from '@/components/stream-amg-player';
import { StreamAmgPreview } from '@/components/stream-amg-preview';
import styles from '../scss/stream-amg-card.module.scss';

class StreamAmgCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isStreamVisible: false,
            isCheckingRestrictions: false,
        };

        this.rootRef = React.createRef();
    }

    onClickRoot = () => {
        const options = {
            widgetParams: {
                flashvars: {
                    ks: false
                }
            }
        };
        this._showStream(options);
    };

    isMemberRequired() {
        const { video } = this.props;

        if (ArrayHelper.isEmpty(video.sysEntryEntitlements)) return true;

        return video.sysEntryEntitlements.some((value) => {
            return value === '*' || value.toLowerCase() === 'members only';
        });
    }

    checkRestriction(isMemberRequired) {
        this.setState({ isCheckingRestrictions: true });

        if (isMemberRequired) {
            return this.checkMemberRestriction();
        }

        return this.checkFreeRestriction();
    }

    checkFreeRestriction() {
        const { video } = this.props;

        return streamApi
            .getKSessionToken(video.entryId)
            .then((response) => {
                return response.data.KSession;
            });
    }

    checkMemberRestriction() {
        const profile = userProfile.getProfile();
        const { video } = this.props;

        if (!userProfile.isLoggedIn()) {
            localStorage.setItem('videoEntryID', video && video.entryId);
            // localStorage.setItem('videoUrl', window.location.href);
            return Promise.reject(new Error('User not logged in'));
        }

        return streamApi
            .getKSessionToken(video.entryId, profile.auth.access_token)
            .then((response) => {
                return response.data.KSession;
            });
    }

    _showStream = (options) => {
        const { video, gtm, playbackType } = this.props;

        if (playbackType === playbackTypes.POPUP) {
            // streamAmgCard.show(video, gtm, options);
        } else if (playbackType === playbackTypes.INLINE) {
            this.setState({ isStreamVisible: true, options });
        }
    };

    _hideStream = () => {
        this.setState({ isStreamVisible: false });
    };

    _getPlayerModel() {
        const { video } = this.props;

        return {
            entryId: video.entryId,
            playerType: playerTypes.EMBED,
            ...this.state.options
        };
    }

    getGtm() {
        const { video, gtm } = this.props;

        return {
            title: gtm?.title || video.title,
            url: gtm?.url || '',
        };
    }

    render() {
        if (!this.props.video) return null;

        const { video, bemList } = this.props;
        const { isStreamVisible, isCheckingRestrictions } = this.state;
        const playerModel = this._getPlayerModel();
        const previewMod = isStreamVisible ? 'is-hidden' : '';
        const bemlist = getBemList(bemList, [previewMod]);
        const gtm = this.getGtm();

        return (
            <div
                className={`${styles.streamAmgCard} ${video && video.entryId}`}
                tabIndex="0"
                role="button"
                aria-label="watch video"
                ref={this.rootRef}
                onClick={this.onClickRoot}
            >
                <StreamAmgPreview model={video} gtm={gtm} bemList={bemlist} />
                {isStreamVisible && <StreamAmgPlayer model={playerModel} gtm={gtm} bemList={bemList} />}
                {isCheckingRestrictions && <BlockSpinner bemList={['light']} />}
            </div>
        );
    }
}

StreamAmgCard.propTypes = {
    video: PropTypes.object,
    playbackType: PropTypes.string,
    bemList: PropTypes.array,
};

export default StreamAmgCard;
