// const path = require('path');
// const withSass = require('@zeit/next-sass');
// const withCSS = require('@zeit/next-css');
//
// module.exports = withCSS(withSass({
//     webpack(config, options) {
//         config.resolve.alias = {
//             ...config.resolve.alias,
//             'style-settings': path.resolve(__dirname, './lib/project/general/scss/settings/index.scss'),
//         }
//         config.module.rules.push({
//             test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
//             use: {
//                 loader: 'url-loader',
//                 options: {
//                     limit: 100000,
//                 },
//             },
//         });
//
//         return config;
//     }
// }));

// const withSass = require('@zeit/next-sass');
//
// module.exports = withSass({
//     cssModules: true,
//     cssLoaderOptions: {
//         importLoaders: 1,
//         localIdentName: '[local]___[hash:base64:5]',
//     },
//     webpack(config, options) {
//         Object.assign(config.resolve.alias, {
//             'style-settings': path.resolve(
//                 __dirname,
//                 './lib/project/general/scss/settings/index.scss'
//             )
//         });
//
//         config.module.rules.push(
//             {
//                 enforce: 'pre',
//                 test: /.scss$/,
//                 loader: 'sass-resources-loader',
//                 options: {
//                     resources: ['./lib/project/general/scss/settings/mixins.scss'],
//                 },
//             },
//         );
//
//         return config;
//     },
// });
module.exports = {
    sassOptions: {
        prependData: '@import \'lib/project/general/scss/settings/index.scss\';',
    },
    target: 'serverless',
};
