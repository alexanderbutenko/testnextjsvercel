export interface ICategory {
    id: string;
    parentId: string | null;
    clientId: string;
    slug: string;
    text: string;
    i18N: string | null;
    lastModified: {
        date: string;
    },
    children: [];
}

export interface ICategoryBySlug {
    id: string;
    text: string;
    targetUrl: string;
}

export interface IArticleContent {
    id: string;
    contentType: string;
    content: string;
    title: string | null;
    image: string | null;
    imageThumbnail: string | null;
    videoThumbnail: string | null;
    publishDate: string | null;
    isHtml: boolean;
    text: null;
    author: null;
    children: null;
    sourceSystemId: null;
    tags: null;
    articleIds: string | null;
    categoryIds: string | null;
    linkedIds: null;
    feedUrl: string | null;
    sponsor: {
        imageUrl: string | null;
        linkUrl: string | null;
    },
    link: string | null;
}

export interface IHeroMedia {
    content: IArticleContent;
    title: string;
    summary: string | null;
}

export interface IAuthor {
    name: string | null;
    title: string | null;
    imageUrl: | null;
    socialHandles: [];
}

export interface IArticleMetaData {
    title: string | null;
    description: string | null;
    ogTitle: string | null;
    ogDescription: string | null;
    ogType: string | null;
    ogImageUrl: string | null;
    twitterCard: string | null;
    twitterSite: string | null;
    twitterCreator: string | null;
}

export interface IArticle {
    id: string;
    publishDate: string;
    tags: null;
    categories: ICategory[];
    displayCategory: ICategory | null;
    slug: string;
    singlePage: boolean;
    heroMedia: IHeroMedia;
    linkedIds: [
        {
            sourceSystem: string;
            sourceSystemId: string;
        }
    ],
    readTimeMinutes: number;
    author: IAuthor;
    auth: {
        loginRequired: boolean;
        roles: [];
    };
    language: string;
    blocked: boolean;
    articleMetaData: IArticleMetaData;
    title: string;
}

export interface IArticleLatestNews {
    id: string;
    category: string;
}

export interface IArticleBySlug extends IArticleLatestNews {
    summary: null;
    content: IArticleContent[];
    latestNewsCarousel: null;
    configurableCarousels: [];
}

export interface IArticlePage {
    article: IArticle;
    statusCode: number;
}

export interface IAllArticles {
    articles: IArticle[];
    categories: {
        [key: string]: ICategory;
    };
}

export interface ILatestNews {
    articles: IArticleLatestNews[];
}
